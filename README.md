# Distribution server

* Install docker and docker-compose
* Run ```docker-compose up -d```. This will start a MySQL database container.
* Navigate to frontend/webclient. Run ```npm run start``` to start the development server for the webclient.
* Navigate to http://localhost:4200. 

# Agents

* Install docker and docker-compose
* Run ```docker run -d tamastarjan/agent:1.0``` to start an agent instance.
* You can run ```docker run -d tamastarjan/agentserial:1.0``` if you are starting the agent on a single core machine.
