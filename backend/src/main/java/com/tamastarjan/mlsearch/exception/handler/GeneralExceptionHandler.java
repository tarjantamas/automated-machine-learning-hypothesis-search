package com.tamastarjan.mlsearch.exception.handler;

import com.tamastarjan.mlsearch.common.api.ReturnCode;
import com.tamastarjan.mlsearch.exception.ApiError;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
@Order
public class GeneralExceptionHandler {

  private static final Logger LOG = LoggerFactory.getLogger(GeneralExceptionHandler.class);

  @ExceptionHandler(Exception.class)
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ResponseBody
  public ApiError handleGeneralException(Exception e) {
    e.printStackTrace();
    System.out.println(e.getCause().toString());
    LOG.error("{}", e);

    return ApiError.builder().code(ReturnCode.INTERNAL_SERVER_ERROR.getCode())
            .message(ReturnCode.INTERNAL_SERVER_ERROR.getMessage()).build();
  }
}
