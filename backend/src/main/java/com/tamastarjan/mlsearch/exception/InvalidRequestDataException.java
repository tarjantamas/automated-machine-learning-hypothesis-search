package com.tamastarjan.mlsearch.exception;

import com.tamastarjan.mlsearch.common.api.ReturnCode;

public class InvalidRequestDataException extends BadRequestWithExplanationException {

  public InvalidRequestDataException(String explanation) {
    super(ReturnCode.INVALID_REQUEST_DATA, explanation);
  }
}
