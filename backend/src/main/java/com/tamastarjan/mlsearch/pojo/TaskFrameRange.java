package com.tamastarjan.mlsearch.pojo;

import com.tamastarjan.mlsearch.entity.SearchTask;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaskFrameRange {

  private static final Integer taskCountPerCpu = 1;

  private Long start;

  private Long end;

  public static TaskFrameRange fromSearchTask(SearchTask searchTask) {
    return new TaskFrameRange(searchTask.getDistributedRange(), searchTask.getSearchTaskSize());
  }

  public List<TaskFrameRange> distribute(List<Integer> applicantCoreCounts) {
    List<TaskFrameRange> result = new ArrayList<>();

    Long currentStart = start;
    Long currentEnd;

    for (Integer coreCount : applicantCoreCounts) {
      currentEnd = Math.min(currentStart + coreCount * taskCountPerCpu, end);
      result.add(new TaskFrameRange(currentStart, currentEnd));
      currentStart = currentEnd;

      if (currentEnd.equals(end)) {
        break;
      }
    }

    return result;
  }
}
