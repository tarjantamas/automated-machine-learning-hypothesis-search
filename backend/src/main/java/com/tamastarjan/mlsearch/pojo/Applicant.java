package com.tamastarjan.mlsearch.pojo;

import com.tamastarjan.mlsearch.entity.Agent;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.async.DeferredResult;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Applicant {

  private Agent agent;

  private DeferredResult<ResponseEntity> deferredResult;

  public Integer getCpuCount() {
    return agent.getCpuCount();
  }
}
