package com.tamastarjan.mlsearch.entity;

import com.tamastarjan.mlsearch.common.db.DbColumnConstants;
import com.tamastarjan.mlsearch.common.db.DbTableConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = DbTableConstants.TASK_RESULT)
public class Result {

  @Id
  @Column(name = DbColumnConstants.ID)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = DbColumnConstants.HYPOTHESIS_INDEX)
  private Long hypothesisIndex;

  @Column(name = DbColumnConstants.MIN)
  private Double min;

  @Column(name = DbColumnConstants.MAX)
  private Double max;

  @Column(name = DbColumnConstants.MEAN)
  private Double mean;

  @Column(name = DbColumnConstants.MEDIAN)
  private Double median;

  @Column(name = DbColumnConstants.STD)
  private Double std;

  @Column(name = DbColumnConstants.HYPOTHESIS, length = 3000)
  private String hypothesis;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = DbColumnConstants.FK_AGENT_ID)
  private Agent agent;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = DbColumnConstants.FK_SEARCH_TASK_ID)
  private SearchTask searchTask;
}
