package com.tamastarjan.mlsearch.entity;

import com.tamastarjan.mlsearch.common.db.DbColumnConstants;
import com.tamastarjan.mlsearch.common.db.DbTableConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = DbTableConstants.AGENT_LOGS)
public class AgentLog {

  @Id
  @Column(name = DbColumnConstants.ID)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = DbColumnConstants.TEXT, length = 3000)
  private String text;

  @OneToOne(fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
  @JoinColumn(name = DbColumnConstants.FK_AGENT_LOG_TRACEBACK_ID)
  private AgentLogTraceback traceback;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = DbColumnConstants.FK_AGENT_ID)
  private Agent agent;
}
