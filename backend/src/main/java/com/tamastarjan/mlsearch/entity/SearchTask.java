package com.tamastarjan.mlsearch.entity;

import com.tamastarjan.mlsearch.common.db.DbColumnConstants;
import com.tamastarjan.mlsearch.common.db.DbTableConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = DbTableConstants.SEARCH_TASKS, uniqueConstraints = {
        @UniqueConstraint(columnNames = DbColumnConstants.FILE_NAME)
})
public class SearchTask {

  @Id
  @Column(name = DbColumnConstants.ID)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = DbColumnConstants.NAME)
  private String name;

  @Column(name = DbColumnConstants.FILE_NAME)
  private String fileName;

  @Column(name = DbColumnConstants.SEARCH_TASK_SIZE)
  private Long searchTaskSize;

  @Column(name = DbColumnConstants.PREFERRED_AGENT_COUNT)
  private Integer preferredAgentCount;

  @Column(name = DbColumnConstants.DISTRIBUTED_RANGE)
  private Long distributedRange;

  @Column(name = DbColumnConstants.COMPLETED_COUNT)
  private Long completedCount;

  @Column(name = DbColumnConstants.SEARCH_START_TIME_MS)
  private Long searchStartTimeMs;

  @Column(name = DbColumnConstants.SEARCH_END_TIME_MS)
  private Long searchEndTimeMs;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = DbColumnConstants.FK_DATASET_ID)
  private Dataset dataset;
}
