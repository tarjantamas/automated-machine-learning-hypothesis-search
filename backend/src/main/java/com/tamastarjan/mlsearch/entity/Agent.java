package com.tamastarjan.mlsearch.entity;

import com.tamastarjan.mlsearch.common.db.DbColumnConstants;
import com.tamastarjan.mlsearch.common.db.DbTableConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = DbTableConstants.AGENTS, uniqueConstraints = {
        @UniqueConstraint(columnNames = DbColumnConstants.UUID)
})
public class Agent {

  @Id
  @Column(name = DbColumnConstants.ID)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = DbColumnConstants.UUID)
  private String uuid;

  @Column(name = DbColumnConstants.IP)
  private String ip;

  @Column(name = DbColumnConstants.STATUS)
  private String status;

  @Column(name = DbColumnConstants.CPU_COUNT)
  private Integer cpuCount;
}
