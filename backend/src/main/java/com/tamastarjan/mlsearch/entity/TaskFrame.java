package com.tamastarjan.mlsearch.entity;

import com.tamastarjan.mlsearch.common.db.DbColumnConstants;
import com.tamastarjan.mlsearch.common.db.DbTableConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = DbTableConstants.TASK_FRAMES)
public class TaskFrame {

  @Id
  @Column(name = DbColumnConstants.ID)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = DbColumnConstants.SEARCH_START)
  private Long searchStart;

  @Column(name = DbColumnConstants.SEARCH_END)
  private Long searchEnd;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = DbColumnConstants.FK_AGENT_ID)
  private Agent agent;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = DbColumnConstants.FK_SEARCH_TASK_ID)
  private SearchTask searchTask;
}
