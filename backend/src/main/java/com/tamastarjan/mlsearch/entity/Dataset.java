package com.tamastarjan.mlsearch.entity;

import com.tamastarjan.mlsearch.common.db.DbColumnConstants;
import com.tamastarjan.mlsearch.common.db.DbTableConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = DbTableConstants.DATASETS, uniqueConstraints = {
        @UniqueConstraint(columnNames = DbColumnConstants.FILE_NAME)
})
public class Dataset {

  @Id
  @Column(name = DbColumnConstants.ID)
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = DbColumnConstants.NAME)
  private String name;

  @Column(name = DbColumnConstants.SIGNATURE)
  private String signature;

  @Column(name = DbColumnConstants.FILE_NAME)
  private String fileName;
}
