package com.tamastarjan.mlsearch.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tamastarjan.mlsearch.common.api.RestApiConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AgentLogDto {

  @JsonProperty(RestApiConstants.ID)
  private Long id;

  @NotEmpty
  @JsonProperty(RestApiConstants.TEXT)
  private String text;

  @NotEmpty
  @JsonProperty(RestApiConstants.AGENT_UUID)
  private String agentUuid;

  @JsonProperty(RestApiConstants.TRACEBACK)
  private AgentLogTracebackDto traceback;
}
