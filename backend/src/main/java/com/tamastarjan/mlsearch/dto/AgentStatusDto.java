package com.tamastarjan.mlsearch.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tamastarjan.mlsearch.common.api.RestApiConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AgentStatusDto {

  @NotEmpty
  @JsonProperty(RestApiConstants.UUID)
  private String uuid;

  @NotEmpty
  @JsonProperty(RestApiConstants.STATUS)
  private String status;
}
