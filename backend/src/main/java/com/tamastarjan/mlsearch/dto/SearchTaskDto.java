package com.tamastarjan.mlsearch.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tamastarjan.mlsearch.common.api.RestApiConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SearchTaskDto {

  @JsonProperty(RestApiConstants.ID)
  private Long id;

  @JsonProperty(RestApiConstants.NAME)
  private String name;

  @JsonProperty(RestApiConstants.FILE_NAME)
  private String fileName;

  @JsonProperty(RestApiConstants.SEARCH_TASK_SIZE)
  private Long searchTaskSize;

  @JsonProperty(RestApiConstants.PREFERRED_AGENT_COUNT)
  private Integer preferredAgentCount;

  @JsonProperty(RestApiConstants.DISTRIBUTED_RANGE)
  private Long distributedRange;

  @JsonProperty(RestApiConstants.COMPLETED_COUNT)
  private Long completedCount;

  @JsonProperty(RestApiConstants.SEARCH_START_TIME_MS)
  private Long searchStartTimeMs;

  @JsonProperty(RestApiConstants.SEARCH_END_TIME_MS)
  private Long searchEndTimeMs;

  @JsonProperty(RestApiConstants.DATASET_ID)
  private Long datasetId;
}
