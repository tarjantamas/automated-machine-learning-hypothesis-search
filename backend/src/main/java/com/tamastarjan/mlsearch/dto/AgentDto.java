package com.tamastarjan.mlsearch.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tamastarjan.mlsearch.common.api.RestApiConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AgentDto {

  @JsonProperty(RestApiConstants.ID)
  private Long id;

  @JsonProperty(RestApiConstants.UUID)
  private String uuid;

  @JsonProperty(RestApiConstants.IP)
  private String ip;

  @JsonProperty(RestApiConstants.STATUS)
  private String status;

  @JsonProperty(RestApiConstants.CPU_COUNT)
  private Integer cpuCount;
}
