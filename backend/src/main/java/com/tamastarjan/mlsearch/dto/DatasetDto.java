package com.tamastarjan.mlsearch.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tamastarjan.mlsearch.common.api.RestApiConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DatasetDto {

  @JsonProperty(RestApiConstants.ID)
  private Long id;

  @JsonProperty(RestApiConstants.NAME)
  private String name;

  @JsonProperty(RestApiConstants.SIGNATURE)
  private String signature;

  @JsonProperty(RestApiConstants.FILE_NAME)
  private String fileName;
}
