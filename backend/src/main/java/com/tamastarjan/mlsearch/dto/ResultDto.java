package com.tamastarjan.mlsearch.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tamastarjan.mlsearch.common.api.RestApiConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResultDto {

  @JsonProperty(RestApiConstants.ID)
  private Long id;

  @NotNull
  @JsonProperty(RestApiConstants.HYPOTHESIS_INDEX)
  private Long hypothesisIndex;

  @NotNull
  @JsonProperty(RestApiConstants.MIN)
  private Double min;

  @NotNull
  @JsonProperty(RestApiConstants.MAX)
  private Double max;

  @NotNull
  @JsonProperty(RestApiConstants.MEAN)
  private Double mean;

  @NotNull
  @JsonProperty(RestApiConstants.MEDIAN)
  private Double median;

  @NotNull
  @JsonProperty(RestApiConstants.STD)
  private Double std;

  @NotEmpty
  @JsonProperty(RestApiConstants.HYPOTHESIS)
  private String hypothesis;

  @NotEmpty
  @JsonProperty(RestApiConstants.AGENT_UUID)
  private String agentUuid;

  @NotNull
  @JsonProperty(RestApiConstants.SEARCH_TASK_ID)
  private Long searchTaskId;
}
