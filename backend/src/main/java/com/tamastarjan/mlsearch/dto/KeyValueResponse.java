package com.tamastarjan.mlsearch.dto;

import java.util.HashMap;

public class KeyValueResponse extends HashMap<String, Object> {

  public KeyValueResponse(String key, Object object) {
    put(key, object);
  }

  public KeyValueResponse() {
  }

  public static KeyValueResponseBuilder builder() {
    return new KeyValueResponseBuilder();
  }

  public static class KeyValueResponseBuilder {
    private KeyValueResponse keyValueResponse = new KeyValueResponse();

    public KeyValueResponseBuilder set(String key, Object value) {
      keyValueResponse.put(key, value);
      return this;
    }

    public KeyValueResponse build() {
      return keyValueResponse;
    }
  }
}
