package com.tamastarjan.mlsearch.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tamastarjan.mlsearch.common.api.RestApiConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaskFrameDto {

  @JsonProperty(RestApiConstants.ID)
  private Long id;

  @JsonProperty(RestApiConstants.SEARCH_START)
  private Long searchStart;

  @JsonProperty(RestApiConstants.SEARCH_END)
  private Long searchEnd;

  @JsonProperty(RestApiConstants.AGENT_UUID)
  private String agentUuid;

  @JsonProperty(RestApiConstants.SEARCH_TASK)
  private SearchTaskDto searchTask;
}
