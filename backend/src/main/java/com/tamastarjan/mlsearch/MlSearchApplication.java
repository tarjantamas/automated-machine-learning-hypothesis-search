package com.tamastarjan.mlsearch;

import com.tamastarjan.mlsearch.config.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({
        FileStorageProperties.class
})
public class MlSearchApplication {

  public static void main(String[] args) {
    SpringApplication.run(MlSearchApplication.class, args);
  }
}
