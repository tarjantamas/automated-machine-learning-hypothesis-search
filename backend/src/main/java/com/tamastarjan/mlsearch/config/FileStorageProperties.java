package com.tamastarjan.mlsearch.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "file")
public class FileStorageProperties {

  private String storageDirectory;

  public String getStorageDirectory() {
    return storageDirectory;
  }

  public void setStorageDirectory(String storageDirectory) {
    this.storageDirectory = storageDirectory;
  }
}
