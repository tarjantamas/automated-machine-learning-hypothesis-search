package com.tamastarjan.mlsearch.common.util;

import com.tamastarjan.mlsearch.exception.InternalServerException;

public class ExceptionUtil {

  public static void throwInternalServerExceptionIf(Boolean condition) {
    if (condition) {
      throw new InternalServerException();
    }
  }
}
