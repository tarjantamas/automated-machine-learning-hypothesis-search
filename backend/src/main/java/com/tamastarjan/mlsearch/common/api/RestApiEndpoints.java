package com.tamastarjan.mlsearch.common.api;

public final class RestApiEndpoints {

  private static final String API_ROOT = "/api/v1";

  public static final String TASK_FRAMES = API_ROOT + "/task-frames";

  public static final String AGENT = API_ROOT + "/agent";

  public static final String AGENTS = API_ROOT + "/agents";

  public static final String SEARCH_TASK = API_ROOT + "/search-task";

  public static final String SEARCH_TASKS = API_ROOT + "/search-tasks";

  public static final String DATASET = API_ROOT + "/dataset";

  public static final String DATASETS = API_ROOT + "/datasets";

  public static final String RESULTS = API_ROOT + "/results";

  public static final String AGENT_LOGS = API_ROOT + "/agent-logs";

  private RestApiEndpoints() {
  }
}
