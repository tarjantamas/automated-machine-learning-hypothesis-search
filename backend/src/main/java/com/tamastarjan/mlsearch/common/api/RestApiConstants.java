package com.tamastarjan.mlsearch.common.api;

public final class RestApiConstants {

  public static final String CODE = "code";

  public static final String MESSAGE = "message";

  public static final String DATA = "data";

  public static final String EXPLANATION = "explanation";

  public static final String ID = "id";

  public static final String NAME = "name";

  public static final String SIGNATURE = "signature";

  public static final String FILE_NAME = "fileName";

  public static final String DATASET_ID = "datasetId";

  public static final String SEARCH_TASK_SIZE = "searchTaskSize";

  public static final String UUID = "uuid";

  public static final String IP = "ip";

  public static final String CPU_COUNT = "cpuCount";

  public static final String PREFERRED_AGENT_COUNT = "preferredAgentCount";

  public static final String DISTRIBUTED_RANGE = "distributedRange";

  public static final String HYPOTHESIS_INDEX = "hypothesisIndex";

  public static final String SEARCH_TASK_ID = "searchTaskId";

  public static final String MIN = "min";

  public static final String MAX = "max";

  public static final String MEAN = "mean";

  public static final String MEDIAN = "median";

  public static final String STD = "std";

  public static final String AGENT_UUID = "agentUuid";

  public static final String STATUS = "status";

  public static final String TEXT = "text";

  public static final String SEARCH_START = "searchStart";

  public static final String SEARCH_END = "searchEnd";

  public static final String SEARCH_TASK = "searchTask";

  public static final String TRACEBACK = "traceback";

  public static final String HYPOTHESIS = "hypothesis";

  public static final String COMPLETED_COUNT = "completedCount";

  public static final String SEARCH_START_TIME_MS = "searchStartTimeMs";

  public static final String SEARCH_END_TIME_MS = "searchEndTimeMs";

  private RestApiConstants() {
  }
}
