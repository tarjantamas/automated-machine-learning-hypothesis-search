package com.tamastarjan.mlsearch.common.api;

public enum ReturnCode {

  MISSING_REQUEST_DATA(100, "The request is missing required data."),

  INVALID_DATA(101, "The request contains invalid data."),

  INVALID_MEDIA_TYPE(103, "The request contains invalid media type."),

  INVALID_METHOD_ARGUMENT(104, "The request contains invalid method arguments"),

  INVALID_REQUEST_DATA(105, "The request contains invalid data."),

  INSUFFICIENT_PRIVILEGES(106, "The caller has insufficient privileges to execute the action."),

  METHOD_NOT_SUPPORTED(108, "Request method not supported."),

  MISSING_REQUEST_PARAM(109, "Request is missing required parameters."),

  MULTIPART_REQUEST_REQUIRED(110, "Request is not a multipart request."),

  INTERNAL_SERVER_ERROR(9999, "Oops! Something unexpectedly went wrong. Please contact the support to fix this issue.");

  private final int code;

  private final String message;

  ReturnCode(int code, String message) {
    this.code = code;
    this.message = message;
  }

  public int getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }
}
