package com.tamastarjan.mlsearch.common.db;

public final class DbTableConstants {

  public static final String DATASETS = "datasets";

  public static final String SEARCH_TASKS = "search_tasks";

  public static final String AGENTS = "agents";

  public static final String TASK_FRAMES = "task_frames";

  public static final String TASK_RESULT = "task_result";

  public static final String AGENT_LOGS = "agent_logs";

  public static final String AGENT_LOGS_TRACEBACK = "agent_logs_traceback";

  private DbTableConstants() {
  }
}
