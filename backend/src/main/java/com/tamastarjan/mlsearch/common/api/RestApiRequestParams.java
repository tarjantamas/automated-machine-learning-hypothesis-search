package com.tamastarjan.mlsearch.common.api;

public final class RestApiRequestParams {

  public static final String FILE = "file";

  public static final String NAME = "name";

  public static final String SEARCH_TASK_SIZE = "search-task-size";

  public static final String DATASET_ID = "dataset-id";

  public static final String AGENT_UUID = "agent-uuid";

  public static final String PREFERRED_AGENT_COUNT = "preferred-agent-count";

  public static final String SEARCH_TASK_ID = "search-task-id";

  private RestApiRequestParams() {
  }
}
