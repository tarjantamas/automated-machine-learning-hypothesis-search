package com.tamastarjan.mlsearch.common.db;

public final class DbColumnConstants {

  public static final String ID = "id";

  public static final String NAME = "name";

  public static final String SIGNATURE = "signature";

  public static final String FILE_NAME = "file_name";

  public static final String FK_DATASET_ID = "fk_dataset_id";

  public static final String SEARCH_TASK_SIZE = "search_task_size";

  public static final String UUID = "uuid";

  public static final String IP = "ip";

  public static final String FK_AGENT_ID = "fk_agent_id";

  public static final String FK_SEARCH_TASK_ID = "fk_search_task_id";

  public static final String SEARCH_START = "search_start";

  public static final String SEARCH_END = "search_end";

  public static final String HYPOTHESIS_INDEX = "hypothesis_index";

  public static final String MIN = "min";

  public static final String MAX = "max";

  public static final String MEAN = "mean";

  public static final String MEDIAN = "median";

  public static final String STD = "std";

  public static final String CPU_COUNT = "cpu_count";

  public static final String DISTRIBUTED_RANGE = "distributed_range";

  public static final String PREFERRED_AGENT_COUNT = "preferred_agent_count";

  public static final String STATUS = "status";

  public static final String TEXT = "text";

  public static final String FK_AGENT_LOG_TRACEBACK_ID = "fk_agent_log_traceback_id";

  public static final String HYPOTHESIS = "hypothesis";

  public static final String COMPLETED_COUNT = "completed_count";

  public static final String SEARCH_START_TIME_MS = "search_start_time";

  public static final String SEARCH_END_TIME_MS = "search_end_time";

  private DbColumnConstants() {
  }
}
