package com.tamastarjan.mlsearch.service;

import com.tamastarjan.mlsearch.config.FileStorageProperties;
import com.tamastarjan.mlsearch.exception.FileStorageException;
import com.tamastarjan.mlsearch.exception.InternalServerException;
import com.tamastarjan.mlsearch.exception.NotFoundWithExplanationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class SearchTaskStorageService {

  private final Path searchTaskStoragePath;

  @Autowired
  public SearchTaskStorageService(FileStorageProperties fileStorageProperties) {
    String baseStorageDirectory = fileStorageProperties.getStorageDirectory();
    searchTaskStoragePath = Paths.get(String.format("%s/searchtasks", baseStorageDirectory))
            .toAbsolutePath().normalize();
    try {
      Files.createDirectories(searchTaskStoragePath);
    } catch (IOException e) {
      throw new FileStorageException("Could not create search task storage directory.");
    }
  }

  public void store(MultipartFile file, String fileName) {
    Path targetLocation = searchTaskStoragePath.resolve(fileName);

    try {
      Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
    } catch (IOException e) {
      throw new InternalServerException(e);
    }
  }

  public Resource loadDatasetAsResource(String fileName) {
    try {
      Path filePath = searchTaskStoragePath.resolve(fileName).normalize();
      Resource resource = new UrlResource(filePath.toUri());
      if (!resource.exists()) {
        throw new NotFoundWithExplanationException("SearchTask with given id is not stored on the server.");
      }
      return resource;
    } catch (MalformedURLException e) {
      throw new InternalServerException(e);
    }
  }
}
