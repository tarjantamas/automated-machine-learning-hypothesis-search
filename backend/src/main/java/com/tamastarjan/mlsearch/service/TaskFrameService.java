package com.tamastarjan.mlsearch.service;

import com.tamastarjan.mlsearch.dto.TaskFrameDto;
import com.tamastarjan.mlsearch.entity.Agent;
import com.tamastarjan.mlsearch.entity.TaskFrame;
import com.tamastarjan.mlsearch.pojo.Applicant;
import com.tamastarjan.mlsearch.validator.db.TaskFrameDbValidatingExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.UUID.randomUUID;

@Service
public class TaskFrameService {

  private static final Logger LOG = LoggerFactory.getLogger(TaskFrameService.class);

  private final TaskFrameDbValidatingExecutor taskFrameDbValidatingExecutor;

  private final AgentService agentService;

  private final SearchTaskService searchTaskService;

  private final Set<Applicant> taskApplicants = ConcurrentHashMap.newKeySet();

  public TaskFrameService(TaskFrameDbValidatingExecutor taskFrameDbValidatingExecutor, AgentService agentService, @Lazy SearchTaskService searchTaskService) {
    this.taskFrameDbValidatingExecutor = taskFrameDbValidatingExecutor;
    this.agentService = agentService;
    this.searchTaskService = searchTaskService;
  }

  public void createAgentApplication(String agentUuid, DeferredResult<ResponseEntity> deferredResult) {
    Applicant applicant = new Applicant();

    Agent agent = agentService.findAgentByUuid(agentUuid);
    applicant.setAgent(agent);
    applicant.setDeferredResult(deferredResult);

    taskApplicants.add(applicant);

    String tempId = randomUUID().toString();
    LOG.info("Adding applicant, temporary uuid: " + tempId);
    deferredResult.onCompletion(() -> {
      LOG.info("Removing applicant, temporary uuid: " + tempId);
      taskApplicants.remove(applicant);
    });

    if (searchTaskService.searchTasksAvailable()) {
      searchTaskService.distributeTaskFrames();
    }
  }

  public Boolean applicantsAvailable() {
    return !taskApplicants.isEmpty();
  }

  public Set<Applicant> getApplicants() {
    return taskApplicants;
  }

  @Transactional
  public List<TaskFrame> saveAll(List<TaskFrame> taskFrames) {
    return taskFrameDbValidatingExecutor.validateAndExecuteSaveAllRequest(taskFrames);
  }

  public void respondToApplicant(Applicant applicant, TaskFrameDto taskFrameDto) {
    if (!taskApplicants.contains(applicant)) {
      throw new RuntimeException("Applicant no longer available.");
    }
    applicant.getDeferredResult().setResult(ResponseEntity.ok(taskFrameDto));
  }
}
