package com.tamastarjan.mlsearch.service;

import com.tamastarjan.mlsearch.dto.DatasetDto;
import com.tamastarjan.mlsearch.entity.Dataset;
import com.tamastarjan.mlsearch.repository.DatasetRepository;
import com.tamastarjan.mlsearch.validator.db.DatasetDbValidatingExecutor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DatasetService {

  private final DatasetDbValidatingExecutor datasetDbValidatingExecutor;

  private final DatasetRepository datasetRepository;

  private final ModelMapper modelMapper;

  public DatasetService(DatasetDbValidatingExecutor datasetDbValidatingExecutor, DatasetRepository datasetRepository, ModelMapper modelMapper) {
    this.datasetDbValidatingExecutor = datasetDbValidatingExecutor;
    this.datasetRepository = datasetRepository;
    this.modelMapper = modelMapper;
  }

  @Transactional
  public DatasetDto create(DatasetDto datasetDto) {
    Dataset dataset = datasetDbValidatingExecutor.validateAndExecuteCreateRequest(datasetDto);

    return modelMapper.map(dataset, DatasetDto.class);
  }

  @Transactional(readOnly = true)
  public DatasetDto findById(Long datasetId) {
    Dataset dataset = datasetDbValidatingExecutor.validateAndExecuteFindByIdRequest(datasetId);

    return modelMapper.map(dataset, DatasetDto.class);
  }

  @Transactional(readOnly = true)
  public Page<DatasetDto> findAll(Pageable pageable) {
    return datasetRepository.findAll(pageable)
            .map(dataset -> modelMapper.map(dataset, DatasetDto.class));
  }
}
