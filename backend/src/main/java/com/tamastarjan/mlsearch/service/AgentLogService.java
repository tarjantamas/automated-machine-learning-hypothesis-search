package com.tamastarjan.mlsearch.service;

import com.tamastarjan.mlsearch.dto.AgentLogDto;
import com.tamastarjan.mlsearch.entity.AgentLog;
import com.tamastarjan.mlsearch.repository.AgentLogRepository;
import com.tamastarjan.mlsearch.validator.db.AgentLogDbValidatingExecutor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AgentLogService {

  private final AgentLogDbValidatingExecutor agentLogDbValidatingExecutor;

  private final AgentLogRepository agentLogRepository;

  private final ModelMapper modelMapper;

  public AgentLogService(AgentLogDbValidatingExecutor agentLogDbValidatingExecutor, AgentLogRepository agentLogRepository, ModelMapper modelMapper) {
    this.agentLogDbValidatingExecutor = agentLogDbValidatingExecutor;
    this.agentLogRepository = agentLogRepository;
    this.modelMapper = modelMapper;
  }

  @Transactional
  public AgentLogDto create(AgentLogDto agentLogDto) {
    AgentLog agentLog = agentLogDbValidatingExecutor.validateAndExecuteCreateRequest(agentLogDto);

    agentLogDto = modelMapper.map(agentLog, AgentLogDto.class);
    agentLogDto.setAgentUuid(agentLog.getAgent().getUuid());

    return agentLogDto;
  }

  @Transactional(readOnly = true)
  public Page<AgentLogDto> findAll(Pageable pageable) {
    Page<AgentLog> page = agentLogRepository.findAll(pageable);

    return mapAgentLogPageToDto(page);
  }

  @Transactional(readOnly = true)
  public Page<AgentLogDto> findByAgentUuid(String agentUuid, Pageable pageable) {
    Page<AgentLog> page = agentLogDbValidatingExecutor.validateAndExecuteFindByAgentUuidRequest(agentUuid, pageable);

    return mapAgentLogPageToDto(page);
  }

  private Page<AgentLogDto> mapAgentLogPageToDto(Page<AgentLog> page) {
    return page.map(agentLog -> modelMapper.map(agentLog, AgentLogDto.class));
  }
}
