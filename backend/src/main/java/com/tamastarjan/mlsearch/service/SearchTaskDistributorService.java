package com.tamastarjan.mlsearch.service;

import com.tamastarjan.mlsearch.dto.TaskFrameDto;
import com.tamastarjan.mlsearch.entity.SearchTask;
import com.tamastarjan.mlsearch.entity.TaskFrame;
import com.tamastarjan.mlsearch.pojo.Applicant;
import com.tamastarjan.mlsearch.pojo.TaskFrameRange;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SearchTaskDistributorService {

  private final TaskFrameService taskFrameService;

  private final SearchTaskService searchTaskService;

  private final ModelMapper modelMapper;

  public SearchTaskDistributorService(TaskFrameService taskFrameService, @Lazy SearchTaskService searchTaskService, ModelMapper modelMapper) {
    this.taskFrameService = taskFrameService;
    this.searchTaskService = searchTaskService;
    this.modelMapper = modelMapper;
  }

  public void distributeTaskFrames(SearchTask searchTask) {
    if (!taskFrameService.applicantsAvailable()) {
      return;
    }

    List<Applicant> applicants = new ArrayList<>(taskFrameService.getApplicants());
    List<TaskFrameRange> taskFrameRanges = getTaskFrameRanges(searchTask, applicants);
    List<TaskFrame> taskFrames = createTaskFrames(searchTask, taskFrameRanges);
    distributeTaskFrames(searchTask, taskFrames, applicants);
  }

  private List<TaskFrame> createTaskFrames(SearchTask searchTask, List<TaskFrameRange> taskFrameRanges) {
    List<TaskFrame> taskFrames = new ArrayList<>();

    for (TaskFrameRange taskFrameRange : taskFrameRanges) {
      taskFrames.add(buildTaskFrame(searchTask, taskFrameRange));
    }

    return taskFrames;
  }

  private TaskFrame buildTaskFrame(SearchTask searchTask, TaskFrameRange taskFrameRange) {
    return TaskFrame.builder()
            .searchTask(searchTask)
            .searchStart(taskFrameRange.getStart())
            .searchEnd(taskFrameRange.getEnd())
            .build();
  }

  private List<TaskFrameRange> getTaskFrameRanges(SearchTask searchTask, List<Applicant> applicants) {
    List<Integer> applicantCoreCounts = getApplicantCoreCounts(applicants);
    TaskFrameRange remainingRange = TaskFrameRange.fromSearchTask(searchTask);

    return remainingRange.distribute(applicantCoreCounts);
  }

  private void distributeTaskFrames(SearchTask searchTask, List<TaskFrame> taskFrames, List<Applicant> applicants) {
    setTaskFrameAgents(taskFrames, applicants);
    taskFrames = taskFrameService.saveAll(taskFrames);
    updateSearchTask(searchTask, taskFrames);
    sendTaskFramesToApplicants(taskFrames, applicants);
  }

  private void updateSearchTask(SearchTask searchTask, List<TaskFrame> taskFrames) {
    searchTask.setDistributedRange(getLastTaskFrame(taskFrames).getSearchEnd());
    searchTaskService.update(searchTask);
  }

  private void sendTaskFramesToApplicants(List<TaskFrame> taskFrames, List<Applicant> applicants) {
    for (int i = 0; i < taskFrames.size(); i++) {
      TaskFrame taskFrame = taskFrames.get(i);
      Applicant applicant = applicants.get(i);

      taskFrameService.respondToApplicant(applicant, modelMapper.map(taskFrame, TaskFrameDto.class));
    }
  }

  private void setTaskFrameAgents(List<TaskFrame> taskFrames, List<Applicant> applicants) {
    for (int i = 0; i < taskFrames.size(); i++) {
      TaskFrame taskFrame = taskFrames.get(i);
      Applicant applicant = applicants.get(i);

      taskFrame.setAgent(applicant.getAgent());
    }
  }

  private TaskFrame getLastTaskFrame(List<TaskFrame> taskFrames) {
    return taskFrames.get(taskFrames.size() - 1);
  }

  private List<Integer> getApplicantCoreCounts(List<Applicant> applicants) {
    return applicants.stream().map(Applicant::getCpuCount).collect(Collectors.toList());
  }
}
