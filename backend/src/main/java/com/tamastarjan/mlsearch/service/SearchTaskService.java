package com.tamastarjan.mlsearch.service;

import com.tamastarjan.mlsearch.dto.SearchTaskDto;
import com.tamastarjan.mlsearch.entity.SearchTask;
import com.tamastarjan.mlsearch.repository.SearchTaskRepository;
import com.tamastarjan.mlsearch.validator.db.SearchTaskDbValidatingExecutor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SearchTaskService {

  private final SearchTaskDbValidatingExecutor searchTaskDbValidatingExecutor;

  private final SearchTaskRepository searchTaskRepository;

  private final SearchTaskDistributorService searchTaskDistributorService;

  private final ModelMapper modelMapper;

  public SearchTaskService(SearchTaskDbValidatingExecutor searchTaskDbValidatingExecutor, SearchTaskRepository searchTaskRepository, SearchTaskDistributorService searchTaskDistributorService, ModelMapper modelMapper) {
    this.searchTaskDbValidatingExecutor = searchTaskDbValidatingExecutor;
    this.searchTaskRepository = searchTaskRepository;
    this.searchTaskDistributorService = searchTaskDistributorService;
    this.modelMapper = modelMapper;
  }

  @Transactional
  public SearchTaskDto create(SearchTaskDto searchTaskDto) {
    SearchTask searchTask = searchTaskDbValidatingExecutor.validateAndExecuteCreateRequest(searchTaskDto);

    return modelMapper.map(searchTask, SearchTaskDto.class);
  }

  @Transactional(readOnly = true)
  public SearchTaskDto findById(Long id) {
    SearchTask searchTask = searchTaskDbValidatingExecutor.validateAndExecuteFindByIdRequest(id);

    return modelMapper.map(searchTask, SearchTaskDto.class);
  }

  @Transactional(readOnly = true)
  public Page<SearchTaskDto> findAll(Pageable pageable) {
    return searchTaskRepository.findAll(pageable)
            .map(searchTask -> modelMapper.map(searchTask, SearchTaskDto.class));
  }

  @Transactional(readOnly = true)
  public Boolean searchTasksAvailable() {
    return !searchTaskRepository.findUnfinishedSearchTasks().isEmpty();
  }

  @Transactional
  public void distributeTaskFrames() {
    List<SearchTask> searchTasks = searchTaskRepository.findUnfinishedSearchTasks();

    for (SearchTask searchTask : searchTasks) {
      searchTaskDistributorService.distributeTaskFrames(searchTask);
    }
  }

  @Transactional(isolation = Isolation.SERIALIZABLE)
  public void update(SearchTask searchTask) {
    searchTaskDbValidatingExecutor.validateAndExecuteUpdateRequest(searchTask);
  }
}
