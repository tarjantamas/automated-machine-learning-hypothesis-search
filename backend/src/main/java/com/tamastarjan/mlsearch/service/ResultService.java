package com.tamastarjan.mlsearch.service;

import com.tamastarjan.mlsearch.dto.ResultDto;
import com.tamastarjan.mlsearch.entity.Result;
import com.tamastarjan.mlsearch.repository.ResultRepository;
import com.tamastarjan.mlsearch.validator.db.ResultDbValidatingExecutor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ResultService {

  private final ResultDbValidatingExecutor resultDbValidatingExecutor;

  private final ResultRepository resultRepository;

  private final ModelMapper modelMapper;

  public ResultService(ResultDbValidatingExecutor resultDbValidatingExecutor, ResultRepository resultRepository, ModelMapper modelMapper) {
    this.resultDbValidatingExecutor = resultDbValidatingExecutor;
    this.resultRepository = resultRepository;
    this.modelMapper = modelMapper;
  }

  @Transactional(isolation = Isolation.SERIALIZABLE, propagation = Propagation.REQUIRES_NEW)
  public ResultDto create(ResultDto resultDto) {
    Result result = resultDbValidatingExecutor.validateAndExecuteCreateRequest(resultDto);

    resultDto.setId(result.getId());

    return resultDto;
  }

  @Transactional(readOnly = true)
  public Page<ResultDto> findAll(Pageable pageable) {
    Page<Result> page = resultRepository.findAll(pageable);

    return page.map(result -> modelMapper.map(result, ResultDto.class));
  }

  @Transactional(readOnly = true)
  public Page<ResultDto> findBySearchTaskId(Long searchTaskId, Pageable pageable) {
    Page<Result> page = resultDbValidatingExecutor.validateAndExecuteFindBySearchTaskIdRequest(searchTaskId, pageable);

    return page.map(result -> modelMapper.map(result, ResultDto.class));
  }
}
