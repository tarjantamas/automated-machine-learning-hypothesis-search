package com.tamastarjan.mlsearch.service;

import com.tamastarjan.mlsearch.dto.AgentDto;
import com.tamastarjan.mlsearch.dto.AgentStatusDto;
import com.tamastarjan.mlsearch.entity.Agent;
import com.tamastarjan.mlsearch.repository.AgentRepository;
import com.tamastarjan.mlsearch.validator.db.AgentDbValidatingExecutor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static java.util.UUID.randomUUID;

@Service
public class AgentService {

  private final ModelMapper modelMapper;

  private final AgentDbValidatingExecutor agentDbValidatingExecutor;

  private final AgentRepository agentRepository;

  public AgentService(ModelMapper modelMapper, AgentDbValidatingExecutor agentDbValidatingExecutor, AgentRepository agentRepository) {
    this.modelMapper = modelMapper;
    this.agentDbValidatingExecutor = agentDbValidatingExecutor;
    this.agentRepository = agentRepository;
  }

  @Transactional
  public AgentDto create(String ip, Integer cpuCount) {
    AgentDto agentDto = AgentDto.builder()
            .ip(ip)
            .uuid(randomUUID().toString())
            .cpuCount(cpuCount)
            .build();

    Agent agent = agentDbValidatingExecutor.validateAndExecuteCreateRequest(agentDto);

    return modelMapper.map(agent, AgentDto.class);
  }

  @Transactional(readOnly = true)
  public AgentDto findByUuid(String uuid) {
    return modelMapper.map(findAgentByUuid(uuid), AgentDto.class);
  }

  @Transactional(readOnly = true)
  public Agent findAgentByUuid(String uuid) {
    return agentDbValidatingExecutor.validateAndExecuteFindByUuidRequest(uuid);
  }

  @Transactional
  public void updateStatus(AgentStatusDto agentStatusDto) {
    agentDbValidatingExecutor.validateAndExecuteUpdateStatusRequest(agentStatusDto);
  }

  @Transactional(readOnly = true)
  public Page<AgentDto> findAll(Pageable pageable) {
    Page<Agent> page = agentRepository.findAll(pageable);

    return page.map(agent -> modelMapper.map(agent, AgentDto.class));
  }

  @Transactional(readOnly = true)
  public Page<AgentDto> findBySearchTaskId(Long searchTaskId, Pageable pageable) {
    Page<Agent> page = agentDbValidatingExecutor.validateAndExecuteFindBySearchTaskIdRequest(searchTaskId, pageable);

    return page.map(agent -> modelMapper.map(agent, AgentDto.class));
  }
}
