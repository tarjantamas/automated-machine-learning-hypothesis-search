package com.tamastarjan.mlsearch.service;

import com.tamastarjan.mlsearch.config.FileStorageProperties;
import com.tamastarjan.mlsearch.exception.FileStorageException;
import com.tamastarjan.mlsearch.exception.InternalServerException;
import com.tamastarjan.mlsearch.exception.NotFoundWithExplanationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class DatasetStorageService {

  private final Path datasetStoragePath;

  @Autowired
  public DatasetStorageService(FileStorageProperties fileStorageProperties) {
    String baseStorageDirectory = fileStorageProperties.getStorageDirectory();
    datasetStoragePath = Paths.get(String.format("%s/datasets", baseStorageDirectory))
            .toAbsolutePath().normalize();
    try {
      Files.createDirectories(datasetStoragePath);
    } catch (IOException e) {
      throw new FileStorageException("Could not create dataset storage directory.");
    }
  }

  public void store(MultipartFile file, String fileName) {
    Path targetLocation = datasetStoragePath.resolve(fileName);

    try {
      Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
    } catch (IOException e) {
      throw new InternalServerException(e);
    }
  }

  public Resource loadDatasetAsResource(String fileName) {
    try {
      Path filePath = datasetStoragePath.resolve(fileName).normalize();
      Resource resource = new UrlResource(filePath.toUri());
      if (!resource.exists()) {
        throw new NotFoundWithExplanationException("Dataset with given id is not stored on the server.");
      }
      return resource;
    } catch (MalformedURLException e) {
      throw new InternalServerException(e);
    }
  }
}
