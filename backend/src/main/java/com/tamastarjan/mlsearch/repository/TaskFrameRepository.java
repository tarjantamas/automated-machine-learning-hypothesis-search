package com.tamastarjan.mlsearch.repository;

import com.tamastarjan.mlsearch.entity.TaskFrame;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TaskFrameRepository extends JpaRepository<TaskFrame, Long> {

  @Query("SELECT taskFrame FROM TaskFrame taskFrame " +
          "WHERE taskFrame.searchTask.id = ?1 AND" +
          "?2 BETWEEN taskFrame.searchStart AND taskFrame.searchEnd-1")
  Optional<TaskFrame> findBySearchTaskIdAndTaskIndex(Long searchTaskId, Long hypothesisIndex);
}
