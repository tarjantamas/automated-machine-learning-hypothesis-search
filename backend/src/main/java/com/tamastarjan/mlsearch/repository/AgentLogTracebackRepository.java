package com.tamastarjan.mlsearch.repository;

import com.tamastarjan.mlsearch.entity.AgentLogTraceback;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AgentLogTracebackRepository extends JpaRepository<AgentLogTraceback, Long> {
}
