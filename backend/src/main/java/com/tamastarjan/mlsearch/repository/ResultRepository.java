package com.tamastarjan.mlsearch.repository;

import com.tamastarjan.mlsearch.entity.Result;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResultRepository extends JpaRepository<Result, Long> {

  Page<Result> findBySearchTaskId(Long searchTaskId, Pageable pageable);
}
