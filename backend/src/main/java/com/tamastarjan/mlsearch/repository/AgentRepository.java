package com.tamastarjan.mlsearch.repository;

import com.tamastarjan.mlsearch.entity.Agent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AgentRepository extends JpaRepository<Agent, Long> {

  Optional<Agent> findByUuid(String uuid);

  @Query("SELECT DISTINCT agent " +
          "FROM Agent agent, SearchTask searchTask, TaskFrame taskFrame " +
          "WHERE taskFrame.searchTask = searchTask AND " +
          "taskFrame.agent = agent")
  Page<Agent> findBySearchTaskId(Long searchTaskId, Pageable pageable);
}
