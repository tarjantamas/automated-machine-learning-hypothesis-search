package com.tamastarjan.mlsearch.repository;

import com.tamastarjan.mlsearch.entity.AgentLog;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AgentLogRepository extends JpaRepository<AgentLog, Long> {

  Page<AgentLog> findByAgentUuid(String agentUuid, Pageable pageable);
}
