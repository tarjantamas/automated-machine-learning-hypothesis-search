package com.tamastarjan.mlsearch.repository;

import com.tamastarjan.mlsearch.entity.SearchTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SearchTaskRepository extends JpaRepository<SearchTask, Long> {

  Boolean existsByFileName(String fileName);

  @Query("SELECT searchTask FROM SearchTask searchTask WHERE searchTask.distributedRange < searchTask.searchTaskSize")
  List<SearchTask> findUnfinishedSearchTasks();
}
