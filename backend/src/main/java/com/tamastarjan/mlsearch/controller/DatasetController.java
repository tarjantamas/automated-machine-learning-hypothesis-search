package com.tamastarjan.mlsearch.controller;

import com.tamastarjan.mlsearch.common.api.RestApiEndpoints;
import com.tamastarjan.mlsearch.dto.DatasetDto;
import com.tamastarjan.mlsearch.service.DatasetService;
import com.tamastarjan.mlsearch.service.DatasetStorageService;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.DATASET)
public class DatasetController {

  private final DatasetStorageService datasetStorageService;

  private final DatasetService datasetService;

  public DatasetController(DatasetStorageService datasetStorageService, DatasetService datasetService) {
    this.datasetStorageService = datasetStorageService;
    this.datasetService = datasetService;
  }


  @GetMapping(value = "/download/{id}", produces = TEXT_PLAIN_VALUE)
  public ResponseEntity<Resource> downloadDataset(@PathVariable("id") Long datasetId) {
    DatasetDto datasetDto = datasetService.findById(datasetId);
    Resource resource = datasetStorageService.loadDatasetAsResource(datasetDto.getFileName());

    return ResponseEntity.ok()
            .contentType(MediaType.TEXT_PLAIN)
            .header(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=\"%s\"", datasetDto.getName()))
            .body(resource);
  }

  @GetMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<DatasetDto> findById(@PathVariable Long id) {
    return ResponseEntity.ok(datasetService.findById(id));
  }
}
