package com.tamastarjan.mlsearch.controller;

import com.tamastarjan.mlsearch.common.api.RestApiConstants;
import com.tamastarjan.mlsearch.common.api.RestApiEndpoints;
import com.tamastarjan.mlsearch.common.api.RestApiRequestParams;
import com.tamastarjan.mlsearch.dto.AgentLogDto;
import com.tamastarjan.mlsearch.dto.KeyValueResponse;
import com.tamastarjan.mlsearch.service.AgentLogService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(RestApiEndpoints.AGENT_LOGS)
public class AgentLogsController {

  private final AgentLogService agentLogService;

  public AgentLogsController(AgentLogService agentLogService) {
    this.agentLogService = agentLogService;
  }

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<KeyValueResponse> create(@RequestBody AgentLogDto agentLogDto) {
    agentLogDto = agentLogService.create(agentLogDto);

    return ResponseEntity.ok(new KeyValueResponse(RestApiConstants.ID, agentLogDto.getId()));
  }

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Page<AgentLogDto>> findAll(Pageable pageable) {
    return ResponseEntity.ok(agentLogService.findAll(pageable));
  }

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, params = { RestApiRequestParams.AGENT_UUID })
  public ResponseEntity<Page<AgentLogDto>> findByAgentUuid(@RequestParam(RestApiRequestParams.AGENT_UUID) String agentUuid,
                                                           Pageable pageable) {
    return ResponseEntity.ok(agentLogService.findByAgentUuid(agentUuid, pageable));
  }
}
