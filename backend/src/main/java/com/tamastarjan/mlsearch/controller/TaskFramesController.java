package com.tamastarjan.mlsearch.controller;

import com.tamastarjan.mlsearch.common.api.RestApiEndpoints;
import com.tamastarjan.mlsearch.common.api.RestApiRequestParams;
import com.tamastarjan.mlsearch.dto.KeyValueResponse;
import com.tamastarjan.mlsearch.service.TaskFrameService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

@RestController
@RequestMapping(RestApiEndpoints.TASK_FRAMES)
public class TaskFramesController {

  private final Long timeOutInMS = 1000L * 15L;

  private final TaskFrameService taskFrameService;

  public TaskFramesController(TaskFrameService taskFrameService) {
    this.taskFrameService = taskFrameService;
  }

  @GetMapping(value = "/apply-for-task-frame", params = {RestApiRequestParams.AGENT_UUID})
  public DeferredResult<ResponseEntity> applyForTaskFrame(
          @RequestParam(RestApiRequestParams.AGENT_UUID) String agentUuid) {

    ResponseEntity<KeyValueResponse> timeOutResponse = ResponseEntity.ok(new KeyValueResponse("message", "timeout"));
    DeferredResult<ResponseEntity> deferredResult = new DeferredResult<>(timeOutInMS, timeOutResponse);

    taskFrameService.createAgentApplication(agentUuid, deferredResult);

    return deferredResult;
  }
}
