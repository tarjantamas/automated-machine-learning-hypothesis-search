package com.tamastarjan.mlsearch.controller;

import com.tamastarjan.mlsearch.common.api.RestApiEndpoints;
import com.tamastarjan.mlsearch.common.api.RestApiRequestParams;
import com.tamastarjan.mlsearch.dto.SearchTaskDto;
import com.tamastarjan.mlsearch.service.SearchTaskService;
import com.tamastarjan.mlsearch.service.SearchTaskStorageService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import static java.util.UUID.randomUUID;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.SEARCH_TASKS)
public class SearchTasksController {

  private final SearchTaskStorageService searchTaskStorageService;

  private final SearchTaskService searchTaskService;

  public SearchTasksController(SearchTaskStorageService searchTaskStorageService, SearchTaskService searchTaskService) {
    this.searchTaskStorageService = searchTaskStorageService;
    this.searchTaskService = searchTaskService;
  }

  @PostMapping(value = "/search-task-upload")
  public ResponseEntity<SearchTaskDto> uploadSearchTask(@RequestParam(RestApiRequestParams.FILE) MultipartFile file,
                                                        @RequestParam(RestApiRequestParams.NAME) String name,
                                                        @RequestParam(RestApiRequestParams.DATASET_ID) Long datasetId,
                                                        @RequestParam(RestApiRequestParams.SEARCH_TASK_SIZE) Long searchTaskSize,
                                                        @RequestParam(RestApiRequestParams.PREFERRED_AGENT_COUNT) Integer preferredAgentCount) {
    SearchTaskDto searchTaskDto = searchTaskService.create(SearchTaskDto.builder()
            .fileName(randomUUID().toString())
            .name(name)
            .searchTaskSize(searchTaskSize)
            .datasetId(datasetId)
            .completedCount(0L)
            .searchStartTimeMs(System.currentTimeMillis())
            .preferredAgentCount(preferredAgentCount)
            .build());

    searchTaskStorageService.store(file, searchTaskDto.getFileName());

    searchTaskService.distributeTaskFrames();

    return ResponseEntity.ok(searchTaskDto);
  }

  @GetMapping(produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<Page<SearchTaskDto>> findAll(Pageable pageable) {
    return ResponseEntity.ok(searchTaskService.findAll(pageable));
  }
}
