package com.tamastarjan.mlsearch.controller;

import com.tamastarjan.mlsearch.common.api.RestApiEndpoints;
import com.tamastarjan.mlsearch.common.api.RestApiRequestParams;
import com.tamastarjan.mlsearch.dto.KeyValueResponse;
import com.tamastarjan.mlsearch.dto.ResultDto;
import com.tamastarjan.mlsearch.service.ResultService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(RestApiEndpoints.RESULTS)
public class ResultsController {

  private final ResultService resultService;

  public ResultsController(ResultService resultService) {
    this.resultService = resultService;
  }

  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<KeyValueResponse> create(@RequestBody ResultDto resultDto) {
    resultDto = resultService.create(resultDto);

    return ResponseEntity.ok(new KeyValueResponse("id", resultDto.getId()));
  }

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Page<ResultDto>> findAll(Pageable pageable) {
    return ResponseEntity.ok(resultService.findAll(pageable));
  }

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, params = { RestApiRequestParams.SEARCH_TASK_ID })
  public ResponseEntity<Page<ResultDto>> findBySearchTaskId(@RequestParam(RestApiRequestParams.SEARCH_TASK_ID) Long searchTaskId,
                                                            Pageable pageable) {
    return ResponseEntity.ok(resultService.findBySearchTaskId(searchTaskId, pageable));
  }
}
