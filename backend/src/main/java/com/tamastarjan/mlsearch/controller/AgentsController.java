package com.tamastarjan.mlsearch.controller;

import com.tamastarjan.mlsearch.common.api.RestApiEndpoints;
import com.tamastarjan.mlsearch.common.api.RestApiRequestParams;
import com.tamastarjan.mlsearch.dto.AgentDto;
import com.tamastarjan.mlsearch.service.AgentService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(RestApiEndpoints.AGENTS)
public class AgentsController {

  private final AgentService agentService;

  public AgentsController(AgentService agentService) {
    this.agentService = agentService;
  }

  @PostMapping(value = "/register", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<AgentDto> registerAgent(@RequestBody AgentDto agentDto, HttpServletRequest request) {
    String ip = request.getRemoteAddr();

    agentDto = agentService.create(ip, agentDto.getCpuCount());

    return ResponseEntity.ok(agentDto);
  }

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Page<AgentDto>> findAll(Pageable pageable) {
    return ResponseEntity.ok(agentService.findAll(pageable));
  }

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, params = { RestApiRequestParams.SEARCH_TASK_ID })
  public ResponseEntity<Page<AgentDto>> findBySearchTaskId(@RequestParam(RestApiRequestParams.SEARCH_TASK_ID) Long searchTaskId,
                                                           Pageable pageable) {
    return ResponseEntity.ok(agentService.findBySearchTaskId(searchTaskId, pageable));
  }
}
