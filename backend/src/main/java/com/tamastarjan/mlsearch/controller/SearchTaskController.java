package com.tamastarjan.mlsearch.controller;

import com.tamastarjan.mlsearch.common.api.RestApiEndpoints;
import com.tamastarjan.mlsearch.dto.SearchTaskDto;
import com.tamastarjan.mlsearch.service.SearchTaskService;
import com.tamastarjan.mlsearch.service.SearchTaskStorageService;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.TEXT_PLAIN_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.SEARCH_TASK)
public class SearchTaskController {

  private final SearchTaskStorageService searchTaskStorageService;

  private final SearchTaskService searchTaskService;

  public SearchTaskController(SearchTaskStorageService searchTaskStorageService, SearchTaskService searchTaskService) {
    this.searchTaskStorageService = searchTaskStorageService;
    this.searchTaskService = searchTaskService;
  }

  @GetMapping(value = "/download/{id}", produces = TEXT_PLAIN_VALUE)
  public ResponseEntity<Resource> downloadSearchTask(@PathVariable("id") Long searchTaskId) {
    String fileName = searchTaskService.findById(searchTaskId).getFileName();
    Resource resource = searchTaskStorageService.loadDatasetAsResource(fileName);

    return ResponseEntity.ok()
            .contentType(MediaType.TEXT_PLAIN)
            .header(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=\"%s\"", resource.getFilename()))
            .body(resource);
  }

  @GetMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<SearchTaskDto> findById(@PathVariable Long id) {
    return ResponseEntity.ok(searchTaskService.findById(id));
  }
}
