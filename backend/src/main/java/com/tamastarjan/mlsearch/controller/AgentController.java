package com.tamastarjan.mlsearch.controller;

import com.tamastarjan.mlsearch.common.api.RestApiEndpoints;
import com.tamastarjan.mlsearch.dto.AgentStatusDto;
import com.tamastarjan.mlsearch.service.AgentService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(RestApiEndpoints.AGENT)
public class AgentController {

  private final AgentService agentService;

  public AgentController(AgentService agentService) {
    this.agentService = agentService;
  }

  @PutMapping(value = "/update-status", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
  public void updateStatus(@RequestBody AgentStatusDto agentStatusDto) {
    agentService.updateStatus(agentStatusDto);
  }
}
