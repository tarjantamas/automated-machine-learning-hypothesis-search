package com.tamastarjan.mlsearch.controller;

import com.tamastarjan.mlsearch.common.api.RestApiEndpoints;
import com.tamastarjan.mlsearch.common.api.RestApiRequestParams;
import com.tamastarjan.mlsearch.dto.DatasetDto;
import com.tamastarjan.mlsearch.service.DatasetService;
import com.tamastarjan.mlsearch.service.DatasetStorageService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import static java.util.UUID.randomUUID;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(RestApiEndpoints.DATASETS)
public class DatasetsController {

  private final DatasetStorageService datasetStorageService;

  private final DatasetService datasetService;

  public DatasetsController(DatasetStorageService datasetStorageService, DatasetService datasetService) {
    this.datasetStorageService = datasetStorageService;
    this.datasetService = datasetService;
  }

  @PostMapping(value = "/dataset-upload")
  public ResponseEntity<DatasetDto> uploadDataset(@RequestParam(RestApiRequestParams.FILE) MultipartFile file,
                                                  @RequestParam(RestApiRequestParams.NAME) String name) {
    DatasetDto datasetMeta = datasetService.create(DatasetDto.builder()
            .fileName(randomUUID().toString())
            .name(name)
            .build());

    datasetStorageService.store(file, datasetMeta.getFileName());

    return ResponseEntity.ok(datasetMeta);
  }

  @GetMapping(produces = APPLICATION_JSON_VALUE)
  public ResponseEntity<Page<DatasetDto>> findAll(Pageable pageable) {
    return ResponseEntity.ok(datasetService.findAll(pageable));
  }
}
