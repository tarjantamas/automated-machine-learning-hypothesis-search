package com.tamastarjan.mlsearch.validator.db;

import com.tamastarjan.mlsearch.entity.SearchTask;
import com.tamastarjan.mlsearch.entity.TaskFrame;
import com.tamastarjan.mlsearch.exception.InternalServerException;
import com.tamastarjan.mlsearch.repository.TaskFrameRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskFrameDbValidatingExecutor {

  private final TaskFrameRepository taskFrameRepository;

  public TaskFrameDbValidatingExecutor(TaskFrameRepository taskFrameRepository) {
    this.taskFrameRepository = taskFrameRepository;
  }

  public List<TaskFrame> validateAndExecuteSaveAllRequest(List<TaskFrame> taskFrames) {
    validateNotEmpty(taskFrames);
    validateSameSearchTask(taskFrames);

    return taskFrameRepository.saveAll(taskFrames);
  }

  private void validateSameSearchTask(List<TaskFrame> taskFrames) {
    SearchTask searchTask = taskFrames.get(0).getSearchTask();
    for (int i = 1; i < taskFrames.size(); i++) {
      if (searchTask != taskFrames.get(i).getSearchTask()) {
        throw new InternalServerException();
      }
    }
  }

  private void validateNotEmpty(List<TaskFrame> taskFrames) {
    if (taskFrames.isEmpty()) {
      throw new InternalServerException();
    }
  }
}
