package com.tamastarjan.mlsearch.validator.db;

import com.tamastarjan.mlsearch.common.util.ExceptionUtil;
import com.tamastarjan.mlsearch.dto.DatasetDto;
import com.tamastarjan.mlsearch.entity.Dataset;
import com.tamastarjan.mlsearch.exception.NotFoundWithExplanationException;
import com.tamastarjan.mlsearch.repository.DatasetRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class DatasetDbValidatingExecutor {

  private final DatasetRepository datasetRepository;

  private final ModelMapper modelMapper;

  public DatasetDbValidatingExecutor(DatasetRepository datasetRepository, ModelMapper modelMapper) {
    this.datasetRepository = datasetRepository;
    this.modelMapper = modelMapper;
  }

  public Dataset validateAndExecuteCreateRequest(DatasetDto datasetDto) {
    ExceptionUtil.throwInternalServerExceptionIf(datasetRepository.existsByFileName(datasetDto.getFileName()));

    return datasetRepository.save(modelMapper.map(datasetDto, Dataset.class));
  }

  public Dataset validateAndExecuteFindByIdRequest(Long id) {
    return datasetRepository.findById(id).orElseThrow(() ->
            new NotFoundWithExplanationException("Dataset with given id not found."));
  }
}
