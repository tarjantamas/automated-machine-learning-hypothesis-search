package com.tamastarjan.mlsearch.validator.db;

import com.tamastarjan.mlsearch.common.api.ReturnCode;
import com.tamastarjan.mlsearch.common.util.ExceptionUtil;
import com.tamastarjan.mlsearch.dto.SearchTaskDto;
import com.tamastarjan.mlsearch.entity.SearchTask;
import com.tamastarjan.mlsearch.exception.BadRequestWithExplanationException;
import com.tamastarjan.mlsearch.exception.NotFoundWithExplanationException;
import com.tamastarjan.mlsearch.repository.DatasetRepository;
import com.tamastarjan.mlsearch.repository.SearchTaskRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
public class SearchTaskDbValidatingExecutor {

  private final SearchTaskRepository searchTaskRepository;

  private final DatasetRepository datasetRepository;

  private final ModelMapper modelMapper;

  public SearchTaskDbValidatingExecutor(SearchTaskRepository searchTaskRepository, DatasetRepository datasetRepository, ModelMapper modelMapper) {
    this.searchTaskRepository = searchTaskRepository;
    this.datasetRepository = datasetRepository;
    this.modelMapper = modelMapper;
  }

  public SearchTask validateAndExecuteCreateRequest(SearchTaskDto searchTaskDto) {
    searchTaskDto.setDistributedRange(0L);

    ExceptionUtil.throwInternalServerExceptionIf(searchTaskRepository.existsByFileName(searchTaskDto.getFileName()));

    validateDatasetExists(searchTaskDto.getDatasetId());

    return searchTaskRepository.save(modelMapper.map(searchTaskDto, SearchTask.class));
  }

  private void validateDatasetExists(Long datasetId) {
    datasetRepository.findById(datasetId).orElseThrow(() ->
            new BadRequestWithExplanationException(ReturnCode.INVALID_REQUEST_DATA,
                    "Provided dataset id does not identify a dataset."));
  }

  public SearchTask validateAndExecuteFindByIdRequest(Long id) {
    return searchTaskRepository.findById(id).orElseThrow(() ->
            new NotFoundWithExplanationException("SearchTask with given id not found."));
  }

  public void validateAndExecuteUpdateRequest(SearchTask searchTask) {
    validateIdPresent(searchTask);
    validateSearchTaskExists(searchTask);

    searchTaskRepository.save(searchTask);
  }

  private void validateSearchTaskExists(SearchTask searchTask) {
    searchTaskRepository.findById(searchTask.getId()).orElseThrow(() ->
            new BadRequestWithExplanationException(ReturnCode.INVALID_REQUEST_DATA,
                    "Search task with given id not found."));
  }

  private void validateIdPresent(SearchTask searchTask) {
    if (searchTask.getId() == null) {
      throw new BadRequestWithExplanationException(ReturnCode.INVALID_REQUEST_DATA,
              "Search task cannot be updated because it is missing an id.");
    }
  }
}
