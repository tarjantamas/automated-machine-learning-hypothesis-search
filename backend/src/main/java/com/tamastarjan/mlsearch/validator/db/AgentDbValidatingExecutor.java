package com.tamastarjan.mlsearch.validator.db;

import com.tamastarjan.mlsearch.common.api.ReturnCode;
import com.tamastarjan.mlsearch.common.util.ExceptionUtil;
import com.tamastarjan.mlsearch.dto.AgentDto;
import com.tamastarjan.mlsearch.dto.AgentStatusDto;
import com.tamastarjan.mlsearch.entity.Agent;
import com.tamastarjan.mlsearch.exception.BadRequestWithExplanationException;
import com.tamastarjan.mlsearch.repository.AgentRepository;
import com.tamastarjan.mlsearch.repository.SearchTaskRepository;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AgentDbValidatingExecutor {

  private final AgentRepository agentRepository;

  private final SearchTaskRepository searchTaskRepository;

  private final ModelMapper modelMapper;

  public AgentDbValidatingExecutor(AgentRepository agentRepository, SearchTaskRepository searchTaskRepository, ModelMapper modelMapper) {
    this.agentRepository = agentRepository;
    this.searchTaskRepository = searchTaskRepository;
    this.modelMapper = modelMapper;
  }

  public Agent validateAndExecuteCreateRequest(AgentDto agentDto) {
    validateAgentWithUUIDNotExists(agentDto);

    return agentRepository.save(modelMapper.map(agentDto, Agent.class));
  }

  private void validateAgentWithUUIDNotExists(AgentDto agentDto) {
    Optional<Agent> optionalAgent = agentRepository.findByUuid(agentDto.getUuid());

    ExceptionUtil.throwInternalServerExceptionIf(optionalAgent.isPresent());
  }

  public Agent validateAndExecuteFindByUuidRequest(String uuid) {
    return validateAgentWithUUIDExists(uuid);
  }

  public void validateAndExecuteUpdateStatusRequest(AgentStatusDto agentStatusDto) {
    Agent agent = validateAgentWithUUIDExists(agentStatusDto.getUuid());

    agent.setStatus(agentStatusDto.getStatus());

    agentRepository.save(agent);
  }

  private Agent validateAgentWithUUIDExists(String uuid) {
    return agentRepository.findByUuid(uuid).orElseThrow(() ->
            new BadRequestWithExplanationException(ReturnCode.INVALID_REQUEST_DATA,
                    "Agent with given uuid not found."));
  }

  public Page<Agent> validateAndExecuteFindBySearchTaskIdRequest(Long searchTaskId, Pageable pageable) {
    validateSearchTaskExists(searchTaskId);

    return agentRepository.findBySearchTaskId(searchTaskId, pageable);
  }

  private void validateSearchTaskExists(Long searchTaskId) {
    searchTaskRepository.findById(searchTaskId).orElseThrow(() ->
            new BadRequestWithExplanationException(ReturnCode.INVALID_REQUEST_DATA,
                    "Search task not found for given search task id."));
  }
}
