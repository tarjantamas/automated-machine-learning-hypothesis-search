package com.tamastarjan.mlsearch.validator.db;

import com.tamastarjan.mlsearch.common.api.ReturnCode;
import com.tamastarjan.mlsearch.dto.ResultDto;
import com.tamastarjan.mlsearch.entity.Agent;
import com.tamastarjan.mlsearch.entity.Result;
import com.tamastarjan.mlsearch.entity.SearchTask;
import com.tamastarjan.mlsearch.entity.TaskFrame;
import com.tamastarjan.mlsearch.exception.BadRequestWithExplanationException;
import com.tamastarjan.mlsearch.repository.AgentRepository;
import com.tamastarjan.mlsearch.repository.ResultRepository;
import com.tamastarjan.mlsearch.repository.SearchTaskRepository;
import com.tamastarjan.mlsearch.repository.TaskFrameRepository;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ResultDbValidatingExecutor {

  private final ResultRepository resultRepository;

  private final AgentRepository agentRepository;

  private final SearchTaskRepository searchTaskRepository;

  private final TaskFrameRepository taskFrameRepository;

  private final ModelMapper modelMapper;

  public ResultDbValidatingExecutor(ResultRepository resultRepository,
                                    AgentRepository agentRepository,
                                    SearchTaskRepository searchTaskRepository,
                                    TaskFrameRepository taskFrameRepository,
                                    ModelMapper modelMapper) {
    this.resultRepository = resultRepository;
    this.agentRepository = agentRepository;
    this.searchTaskRepository = searchTaskRepository;
    this.taskFrameRepository = taskFrameRepository;
    this.modelMapper = modelMapper;
  }

  public Result validateAndExecuteCreateRequest(ResultDto resultDto) {
    Agent agent = validateAgentExists(resultDto);
    SearchTask searchTask = validateSearchTaskExists(resultDto.getSearchTaskId());
    TaskFrame taskFrame = validateTaskFrameContainingTaskIndexExists(resultDto);
    validateIsAgentsTaskFrame(agent, taskFrame);

    searchTask.setCompletedCount(searchTask.getCompletedCount() + 1L);

    if (searchTask.getCompletedCount().equals(searchTask.getSearchTaskSize())) {
      searchTask.setSearchEndTimeMs(System.currentTimeMillis());
    }

    searchTask = searchTaskRepository.save(searchTask);

    Result result = modelMapper.map(resultDto, Result.class);

    result.setSearchTask(searchTask);
    result.setAgent(agent);

    return resultRepository.save(result);
  }

  private void validateIsAgentsTaskFrame(Agent agent, TaskFrame taskFrame) {
    if (!taskFrame.getAgent().getId().equals(agent.getId())) {
      throw new BadRequestWithExplanationException(ReturnCode.INVALID_REQUEST_DATA,
              "Given agent is not authorized to send results for the given task frame.");
    }
  }

  private TaskFrame validateTaskFrameContainingTaskIndexExists(ResultDto resultDto) {
    return taskFrameRepository.findBySearchTaskIdAndTaskIndex(resultDto.getSearchTaskId(), resultDto.getHypothesisIndex()).orElseThrow(() ->
            new BadRequestWithExplanationException(ReturnCode.INVALID_REQUEST_DATA, "Given task is not part of the given task frame."));
  }

  private SearchTask validateSearchTaskExists(Long searchTaskId) {
    return searchTaskRepository.findById(searchTaskId).orElseThrow(() ->
            new BadRequestWithExplanationException(ReturnCode.INVALID_REQUEST_DATA, "Invalid search task id."));
  }

  private Agent validateAgentExists(ResultDto resultDto) {
    return agentRepository.findByUuid(resultDto.getAgentUuid()).orElseThrow(() ->
            new BadRequestWithExplanationException(ReturnCode.INVALID_REQUEST_DATA, "Invalid agent uuid."));
  }

  public Page<Result> validateAndExecuteFindBySearchTaskIdRequest(Long searchTaskId, Pageable pageable) {
    validateSearchTaskExists(searchTaskId);

    return resultRepository.findBySearchTaskId(searchTaskId, pageable);
  }
}
