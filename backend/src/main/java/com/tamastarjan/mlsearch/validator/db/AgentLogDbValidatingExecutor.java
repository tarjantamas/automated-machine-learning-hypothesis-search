package com.tamastarjan.mlsearch.validator.db;

import com.tamastarjan.mlsearch.common.api.ReturnCode;
import com.tamastarjan.mlsearch.dto.AgentLogDto;
import com.tamastarjan.mlsearch.entity.Agent;
import com.tamastarjan.mlsearch.entity.AgentLog;
import com.tamastarjan.mlsearch.entity.AgentLogTraceback;
import com.tamastarjan.mlsearch.exception.BadRequestWithExplanationException;
import com.tamastarjan.mlsearch.repository.AgentLogRepository;
import com.tamastarjan.mlsearch.repository.AgentLogTracebackRepository;
import com.tamastarjan.mlsearch.repository.AgentRepository;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class AgentLogDbValidatingExecutor {

  private final AgentRepository agentRepository;

  private final AgentLogRepository agentLogRepository;

  private final AgentLogTracebackRepository agentLogTracebackRepository;

  private final ModelMapper modelMapper;

  public AgentLogDbValidatingExecutor(AgentRepository agentRepository, AgentLogRepository agentLogRepository, AgentLogTracebackRepository agentLogTracebackRepository, ModelMapper modelMapper) {
    this.agentRepository = agentRepository;
    this.agentLogRepository = agentLogRepository;
    this.agentLogTracebackRepository = agentLogTracebackRepository;
    this.modelMapper = modelMapper;
  }

  public AgentLog validateAndExecuteCreateRequest(AgentLogDto agentLogDto) {
    Agent agent = validateAgentExistsByUuid(agentLogDto.getAgentUuid());

    AgentLog agentLog = modelMapper.map(agentLogDto, AgentLog.class);
    agentLog.setAgent(agent);

    if (agentLogDto.getTraceback() != null) {
      agentLog.setTraceback(new AgentLogTraceback(null, agentLogDto.getTraceback().getText(), agentLog));
    }

    return agentLogRepository.save(agentLog);
  }

  private Agent validateAgentExistsByUuid(String agentUuid) {
    return agentRepository.findByUuid(agentUuid).orElseThrow(() ->
            new BadRequestWithExplanationException(ReturnCode.INVALID_REQUEST_DATA,
                    "Agent with given uuid not found."));
  }

  public Page<AgentLog> validateAndExecuteFindByAgentUuidRequest(String agentUuid, Pageable pageable) {
    validateAgentExistsByUuid(agentUuid);

    return agentLogRepository.findByAgentUuid(agentUuid, pageable);
  }
}
