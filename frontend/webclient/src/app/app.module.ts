import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, ErrorHandler } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';

import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { DatasetModule } from './dataset/dataset.module';
import { HypothesisModule } from './hypothesis/hypothesis.module';
import { AgentModule } from './agent/agent.module';
import { CustomErrorHandler } from './core/handler/error-handler';
import { DocumentationModule } from './documentation/documentation.module';
import { ResultModule } from './result/result.module';
import { AgentLogModule } from './agent-log/agent-log.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    HttpClientModule,
    CoreModule,
    DatasetModule,
    HypothesisModule,
    AgentModule,
    ResultModule,
    AgentLogModule,
    DocumentationModule,
    AppRoutingModule,
  ],
  providers: [
    { provide: ErrorHandler, useClass: CustomErrorHandler }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
