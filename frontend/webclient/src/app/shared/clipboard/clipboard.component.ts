import { Component, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-clipboard',
  templateUrl: './clipboard.component.html',
  styleUrls: ['./clipboard.component.scss']
})
export class ClipboardComponent {

  @ViewChild('textContainer',  { static: true })
  textContainer: ElementRef;

  copyToClipboard(content: string) {
    let htmlInput: HTMLInputElement = this.textContainer.nativeElement;
    htmlInput.value = content;
    htmlInput.select()
    document.execCommand('copy');
  }
}
