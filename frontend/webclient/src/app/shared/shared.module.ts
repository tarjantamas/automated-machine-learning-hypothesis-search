import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { MaterialModule } from '../material/material.module';
import { ContextMenuComponent } from './context-menu/context-menu.component';
import { CmOptionComponent } from './context-menu/cm-option/cm-option.component';
import { OnDocumentClickedComponent } from './on-document-clicked/on-document-clicked.component';
import { ClipboardComponent } from './clipboard/clipboard.component';

const sharedModules = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  MaterialModule
]

@NgModule({
  declarations: [
    FileUploadComponent,
    ContextMenuComponent,
    CmOptionComponent,
    OnDocumentClickedComponent,
    ClipboardComponent
  ],
  imports: [ ...sharedModules ],
  exports: [ 
    ...sharedModules, 
    FileUploadComponent, 
    ContextMenuComponent,
    CmOptionComponent,
    ClipboardComponent
  ],
})
export class SharedModule { }
