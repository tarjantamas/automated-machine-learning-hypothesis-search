import { Component, EventEmitter, Output, HostListener } from '@angular/core';

@Component({
  selector: 'app-on-document-clicked',
  templateUrl: './on-document-clicked.component.html',
  styleUrls: ['./on-document-clicked.component.scss']
})
export class OnDocumentClickedComponent {

  @Output()
  documentClicked: EventEmitter<MouseEvent> = new EventEmitter<MouseEvent>();

  @HostListener('document:click', ['$event'])
  onClick(event: MouseEvent) {
    this.documentClicked.emit(event);
  }
}
