import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FileUploadData } from './file-upload-data.model';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit {

  @Input()
  allowedFileTypes: string[] = [];

  @Input()
  label: string = "";

  @Output()
  loadSuccess: EventEmitter<FileUploadData> = new EventEmitter();

  @Output()
  loadError: EventEmitter<string> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onFileChange(event) {
    const fileTypes = this.allowedFileTypes;
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      let extension = file.name.split('.').pop().toLowerCase();
      let isSuccess = fileTypes.indexOf(extension) > -1;
      if (!isSuccess) {
        this.loadError.emit("Unsuported file type.");
      } else {
        this.loadSuccess.emit(new FileUploadData(file));
      }
    }
  }
}
