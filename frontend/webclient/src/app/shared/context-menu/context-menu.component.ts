import { Component, OnInit, HostListener, ElementRef, NgZone, ChangeDetectorRef, Input } from '@angular/core';

@Component({
  selector: 'app-context-menu',
  templateUrl: './context-menu.component.html',
  styleUrls: ['./context-menu.component.scss']
})
export class ContextMenuComponent implements OnInit {

  isOpen: boolean = false;

  top: number = 0;

  left: number = 0;

  @Input()
  parentRef: ElementRef;

  constructor(
    private elementRef: ElementRef
  ) { }

  ngOnInit() {
  }

  public open(event: MouseEvent) {
    this.isOpen = true;
    this.left = event.pageX - 250;
    this.top = event.pageY;
  }

  public close() {
    this.isOpen = false;
  }

  @HostListener('document:click', ['$event'])
  onClick(event: MouseEvent) {
    if (this.isOpen && !this.isTheTarget(event.target)) {
      this.close();
    }
  }

  isTheTarget(target) {
    let parent = this.parentRef.nativeElement;
    return parent === target || parent.contains(target)
  }
}
