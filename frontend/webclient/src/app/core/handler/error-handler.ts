import { ErrorHandler } from "@angular/core";
import { ToastService } from '../toast.service';

export class CustomErrorHandler implements ErrorHandler {
  
  constructor(
    public toast: ToastService
  ) { }

  handleError(error: any): void {
    if (error.message) {
      error = error.message;
    }
    this.toast.error(error);
    console.error(error)
  }
}