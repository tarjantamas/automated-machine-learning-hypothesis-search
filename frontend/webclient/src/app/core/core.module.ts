import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { NavigationComponent } from './navigation/navigation.component';
import { MaterialModule } from '../material/material.module';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { NotFoundComponent } from './not-found/not-found.component';
import { NBtnLabelComponent } from './navigation/n-btn-label/n-btn-label.component';

@NgModule({
  declarations: [
    NavigationComponent,
    HomeComponent,
    NotFoundComponent,
    NBtnLabelComponent
  ],
  imports: [
    SharedModule,
    RouterModule,
    NgxSpinnerModule,
  ],
  exports: [
    NavigationComponent
  ]
})
export class CoreModule { }
