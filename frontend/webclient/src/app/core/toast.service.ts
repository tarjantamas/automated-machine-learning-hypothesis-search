import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(
    private toastr: ToastrService
  ) { }

  info(msg) {
    this.toastr.info(msg, "Info");
  }

  error(msg) {
    this.toastr.error(msg, "Error");
  }

  warn(msg) {
    this.toastr.warning(msg, "Warning");
  }

  success(msg) {
    this.toastr.success(msg, "Success");
  }
}
