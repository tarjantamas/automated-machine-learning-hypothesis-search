import { Component, OnInit } from '@angular/core';
import { RouteConstants } from 'src/app/routes';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  routeConstants = RouteConstants;

  currentRoute: string = '';

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.currentRoute = window.location.href;
      }
    });
  }

  navigateToRoute(route: string) {
    this.router.navigate([route]);
  }

  isActiveRoute(route: string) {
    return this.currentRoute.search(route) !== -1;
  }
}
