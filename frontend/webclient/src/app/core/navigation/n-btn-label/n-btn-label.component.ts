import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-n-btn-label',
  templateUrl: './n-btn-label.component.html',
  styleUrls: ['./n-btn-label.component.scss']
})
export class NBtnLabelComponent implements OnInit {

  @Input()
  isActive: boolean = false;

  @Input()
  hasChildren: boolean = false;

  @Input()
  iconName: string;

  constructor() { }

  ngOnInit() {
  }

}
