import { Injectable } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  currentRoute: string = "";

  constructor(
    private router: Router
  ) { }

  navigate(commands: any[], navigationExtras?: NavigationExtras) {
    this.currentRoute = commands[0];
    this.router.navigate(commands, navigationExtras);
  }
}
