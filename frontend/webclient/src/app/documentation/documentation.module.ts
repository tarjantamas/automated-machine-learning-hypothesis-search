import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { DocumentationRoutingModule } from './documentation-routing.module';
import { DocumentationComponent } from './documentation/documentation.component';
import { SearchTaskDocumentationComponent } from './search-task-documentation/search-task-documentation.component';


@NgModule({
  declarations: [  
    DocumentationComponent, 
    SearchTaskDocumentationComponent
  ],
  imports: [
    SharedModule,
    DocumentationRoutingModule
  ]
})
export class DocumentationModule { }
