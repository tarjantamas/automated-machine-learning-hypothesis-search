import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteConstants } from '../routes';
import { DocumentationComponent } from './documentation/documentation.component';
import { SearchTaskDocumentationComponent } from './search-task-documentation/search-task-documentation.component';


const routes: Routes = [
  { path: RouteConstants.DOCUMENTATION, component: DocumentationComponent },
  { path: RouteConstants.SEARCH_TASK_DOCUMENTATION, component: SearchTaskDocumentationComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentationRoutingModule { }
