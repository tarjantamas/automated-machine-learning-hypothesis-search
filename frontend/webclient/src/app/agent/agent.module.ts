import { NgModule } from '@angular/core';

import { AgentRoutingModule } from './agent-routing.module';
import { SharedModule } from '../shared/shared.module';
import { AlAgentCardComponent } from './agent-list/al-agent-card/al-agent-card.component';
import { AgentListComponent } from './agent-list/agent-list.component';
import { AgentsPageComponent } from './agents-page/agents-page.component';


@NgModule({
  declarations: [
    AgentListComponent,
    AlAgentCardComponent,
    AgentsPageComponent
  ],
  imports: [
    SharedModule,
    AgentRoutingModule
  ]
})
export class AgentModule { }
