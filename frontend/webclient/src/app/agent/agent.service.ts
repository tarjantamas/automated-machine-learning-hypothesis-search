import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AgentService {

  private agentsEndpoint: string = environment.agentsEndpoint;

  constructor(
    private http: HttpClient
  ) { }

  findAll(): Observable<any> {
    return this.http.get(`${this.agentsEndpoint}?size=100`);
  }

  findBySearchTaskId(searchTaskId: number): Observable<any> {
    return this.http.get(`${this.agentsEndpoint}?search-task-id=${searchTaskId}&size=100`);
  }
}
