import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteConstants } from '../routes';
import { AgentsPageComponent } from './agents-page/agents-page.component';

const routes: Routes = [
  { path: RouteConstants.SEARCH_AGENTS, component: AgentsPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgentRoutingModule { }
