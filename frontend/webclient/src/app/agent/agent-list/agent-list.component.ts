import { Component, OnInit, Input } from '@angular/core';
import { AgentService } from '../agent.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Agent } from '../agent.model';

@Component({
  selector: 'app-agent-list',
  templateUrl: './agent-list.component.html',
  styleUrls: ['./agent-list.component.scss']
})
export class AgentListComponent {

  @Input()
  agents: Agent[];

  cardColors = ['#9D2A24', '#358494', '#DC4539', '#87B7C2', '#F66C5E', '#EBA266'];

  getColor(cardIndex: number) {
    let colorIndex = cardIndex % this.cardColors.length;
    
    return this.cardColors[colorIndex];
  }
}
