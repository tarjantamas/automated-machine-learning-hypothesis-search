import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { Agent } from '../../agent.model';
import { Router } from '@angular/router';
import { RouteConstants } from 'src/app/routes';
import { ContextMenuComponent } from 'src/app/shared/context-menu/context-menu.component';

@Component({
  selector: 'app-al-agent-card',
  templateUrl: './al-agent-card.component.html',
  styleUrls: ['./al-agent-card.component.scss']
})
export class AlAgentCardComponent implements OnInit {

  @ViewChild(ContextMenuComponent, { static: true }) 
  contextMenu: ContextMenuComponent;

  @Input()
  agent: Agent;

  @Input()
  color: string = 'red';

  constructor(
    public elementRef: ElementRef,
    private router: Router
  ) { }

  ngOnInit() {
  }

  navigateToAgentLogsPage() {
    this.router.navigate([RouteConstants.AGENT_LOGS], {queryParams: { agentUuid: this.agent.uuid, page: 0}});
  }
}
