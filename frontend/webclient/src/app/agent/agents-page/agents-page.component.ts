import { Component, OnInit, OnDestroy } from '@angular/core';
import { AgentService } from '../agent.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-agents-page',
  templateUrl: './agents-page.component.html',
  styleUrls: ['./agents-page.component.scss']
})
export class AgentsPageComponent implements OnInit, OnDestroy {

  agentPage: any;

  private queryParamSubscription: Subscription;

  constructor(
    private agentService: AgentService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.initQueryParamSubscription();
  }

  ngOnDestroy() {
    this.queryParamSubscription.unsubscribe();
  }

  private initQueryParamSubscription() {
    this.queryParamSubscription = this.route.queryParams.subscribe(queryParams => {
      let searchTaskId = queryParams.searchTaskId;

      if (searchTaskId) {
        this.loadAgentsBySearchTaskId(searchTaskId);
      } else {
        this.loadAgents();
      }
    });
  }

  private loadAgentsBySearchTaskId(searchTaskId: number) {
    this.spinner.show();
    this.agentService.findBySearchTaskId(searchTaskId).subscribe(agentPage => {
      this.agentPage = agentPage;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
      throw new Error(error.message);
    });
  }

  private loadAgents() {
    this.spinner.show();
    this.agentService.findAll().subscribe(agentPage => {
      this.agentPage = agentPage;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
      throw new Error(error.message);
    });
  }
}
