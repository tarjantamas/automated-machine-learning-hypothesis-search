export class Agent {

  constructor(
    public id: number,
    public uuid: string,
    public ip: string,
    public status: string,
    public cpuCount: number,
  ) { }
}
