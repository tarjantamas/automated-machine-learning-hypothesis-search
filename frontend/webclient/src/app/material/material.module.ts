import { NgModule } from '@angular/core';

import { MatSidenavModule } from '@angular/material/sidenav';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';

const materialModules = [
  MatSidenavModule,
  MatInputModule,
  MatButtonModule,
  MatChipsModule,
  MatIconModule
];

@NgModule({
  imports: [ ...materialModules ],
  exports: [ ...materialModules ]
})
export class MaterialModule { }
