import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FileUploadData } from 'src/app/shared/file-upload/file-upload-data.model';
import { SearchTaskService } from '../search-task.service';
import { ToastService } from 'src/app/core/toast.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-search-task-upload',
  templateUrl: './search-task-upload.component.html',
  styleUrls: ['./search-task-upload.component.scss']
})
export class SearchTaskUploadComponent implements OnInit {

  @Output()
  uploadSuccess: EventEmitter<any> = new EventEmitter();

  searchTaskName: string;

  searchTaskSize: number;

  preferredAgentCount: number;

  @Input()
  datasetId: number;

  fileUploadData: FileUploadData;

  constructor(
    private searchTaskService: SearchTaskService,
    private toast: ToastService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
  }

  uploadSearchTask(fileUploadData: FileUploadData) {
    this.fileUploadData = fileUploadData;
    this.searchTaskName = this.fileUploadData.file.name;
  }

  searchTaskSelectionError(message: any) {
    throw new Error(message);
  }

  upload() {
    this.validateFileUpload();
    this.spinner.show();
    this.searchTaskService.uploadSearchTask(this.fileUploadData.file, this.searchTaskName, this.datasetId, 
                                            this.searchTaskSize, this.preferredAgentCount).subscribe(result => {
      this.uploadSuccess.emit(result);
      this.spinner.hide();
      this.clearFields();
      this.toast.success("Search task has been uploaded.");
    }, error => {
      this.spinner.hide();
      throw new Error(error.message);
    });
  }

  private clearFields() {
    this.searchTaskName = "";
    this.fileUploadData = null;
  }

  private validateFileUpload() {
    if (!this.fileUploadData || !this.searchTaskName || !this.searchTaskSize || !this.datasetId) {
      throw new Error("You have to chose a search task and provide it's size.");
    }
  }

}
