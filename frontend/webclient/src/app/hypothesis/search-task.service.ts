import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { SearchTask } from './search-task.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchTaskService {

  searchTaskEndpoint = environment.searchTaskEndpoint;

  searchTasksEndpoint = environment.searchTasksEndpoint;

  constructor(
    private http: HttpClient
  ) { }

  uploadSearchTask(file, name: string, datasetId: number, searchTaskSize: number, preferredAgentCount: number): Observable<SearchTask> {
    let formData: FormData = new FormData();

    formData.append("file", file);

    return this.http.post<SearchTask>(this.getUploadEndpoint(name, datasetId, searchTaskSize, preferredAgentCount), formData);
  }

  private getUploadEndpoint(name, datasetId, searchTaskSize, preferredAgentCount): string {
    return `${this.searchTasksEndpoint}/search-task-upload?name=${name}` + 
      `&dataset-id=${datasetId}&search-task-size=${searchTaskSize}` + 
      `&preferred-agent-count=${preferredAgentCount}`;
  }

  findAll() {
    return this.http.get(`${this.searchTasksEndpoint}?sort=id,desc`);
  }

  donwloadSearchTask(searchTaskId: number) {
    window.open(`${this.searchTaskEndpoint}/download/${searchTaskId}`, '_blank');
  }

  findById(searchTaskId: any) {
    return this.http.get(`${this.searchTaskEndpoint}/${searchTaskId}`);
  }
}
