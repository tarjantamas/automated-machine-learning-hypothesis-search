export class SearchTask {
  
  constructor(
    public id: number,
    public name: string,
    public fileName: string,
    public preferredAgentCount: number,
    public searchTaskSize: number,
    public distributedRange: number,
    public searchStartTimeMs: number,
    public searchEndTimeMs: number,
    public datasetId: number,
  ) {}
}