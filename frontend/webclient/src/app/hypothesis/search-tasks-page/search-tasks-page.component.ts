import { Component, OnInit } from '@angular/core';
import { SearchTaskService } from '../search-task.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-search-tasks-page',
  templateUrl: './search-tasks-page.component.html',
  styleUrls: ['./search-tasks-page.component.scss']
})
export class SearchTasksPageComponent implements OnInit {

  searchTasksPage: any;

  constructor(
    private searchTaskService: SearchTaskService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
    this.loadSearchTasks();
  }

  private loadSearchTasks() {
    this.spinner.show();
    this.searchTaskService.findAll().subscribe(searchTasksPage => {
      this.searchTasksPage = searchTasksPage;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
      throw new Error(error.message);
    });
  }
}
