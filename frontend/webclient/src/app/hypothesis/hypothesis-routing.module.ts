import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteConstants } from '../routes';
import { HypothesisSearchComponent } from './hypothesis-search/hypothesis-search.component';
import { SearchTasksPageComponent } from './search-tasks-page/search-tasks-page.component';

const routes: Routes = [
  { path: RouteConstants.HYPOTHESIS_SEARCH, component: HypothesisSearchComponent },
  { path: RouteConstants.SEARCH_TASKS, component: SearchTasksPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HypothesisRoutingModule { }
