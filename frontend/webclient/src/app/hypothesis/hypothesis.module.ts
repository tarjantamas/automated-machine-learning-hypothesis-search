import { NgModule } from '@angular/core';

import { HypothesisRoutingModule } from './hypothesis-routing.module';
import { HypothesisSearchComponent } from './hypothesis-search/hypothesis-search.component';
import { SharedModule } from '../shared/shared.module';
import { SearchTaskUploadComponent } from './search-task-upload/search-task-upload.component';
import { SearchTaskListComponent } from './search-task-list/search-task-list.component';
import { StlSearchTaskCardComponent } from './search-task-list/stl-search-task-card/stl-search-task-card.component';
import { SearchTasksPageComponent } from './search-tasks-page/search-tasks-page.component';


@NgModule({
  declarations: [
    HypothesisSearchComponent,
    SearchTaskUploadComponent,
    SearchTaskListComponent,
    StlSearchTaskCardComponent,
    SearchTasksPageComponent
  ],
  imports: [
    SharedModule,
    HypothesisRoutingModule,
  ]
})
export class HypothesisModule { }
