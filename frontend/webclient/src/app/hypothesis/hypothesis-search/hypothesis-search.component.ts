import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Dataset } from 'src/app/dataset/dataset.model';
import { DatasetService } from 'src/app/dataset/dataset.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { RouteConstants } from 'src/app/routes';

@Component({
  selector: 'app-hypothesis-search',
  templateUrl: './hypothesis-search.component.html',
  styleUrls: ['./hypothesis-search.component.scss']
})
export class HypothesisSearchComponent implements OnInit, OnDestroy {

  routeConstants = RouteConstants;

  dataset: Dataset;

  private queryParamSubscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private datasetService: DatasetService,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit() {
    this.subscribeToQueryParams();
  }

  ngOnDestroy() {
    this.queryParamSubscription.unsubscribe();
  }

  private subscribeToQueryParams() {
    this.queryParamSubscription = this.route.queryParams.subscribe(params => {
      if (params.datasetId) {
        this.loadDatasetMeta(params.datasetId);
      }
    });
  }

  private loadDatasetMeta(datasetId: number) {
    this.spinner.show();
    this.datasetService.findById(datasetId).subscribe(dataset => {
      this.dataset = dataset;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
      throw new Error(error.message);
    });
  }
}
