import { Component, Input } from '@angular/core';
import { SearchTask } from '../search-task.model';

@Component({
  selector: 'app-search-task-list',
  templateUrl: './search-task-list.component.html',
  styleUrls: ['./search-task-list.component.scss']
})
export class SearchTaskListComponent {

  @Input()
  searchTasks: SearchTask[];

  cardColors = ['#9D2A24', '#358494', '#DC4539', '#87B7C2', '#F66C5E', '#EBA266'];

  getColor(cardIndex: number) {
    let colorIndex = cardIndex % this.cardColors.length;
    
    return this.cardColors[colorIndex];
  }
}
