import { Component, Input, ElementRef, ViewChild } from '@angular/core';
import { SearchTask } from '../../search-task.model';
import { Router } from '@angular/router';
import { RouteConstants } from 'src/app/routes';
import { ContextMenuComponent } from 'src/app/shared/context-menu/context-menu.component';

@Component({
  selector: 'app-stl-search-task-card',
  templateUrl: './stl-search-task-card.component.html',
  styleUrls: ['./stl-search-task-card.component.scss']
})
export class StlSearchTaskCardComponent {

  @ViewChild(ContextMenuComponent, { static: true }) 
  contextMenu: ContextMenuComponent;

  @Input()
  searchTask: SearchTask;

  @Input()
  color: string = 'red';

  constructor(
    public elementRef: ElementRef,
    private router: Router
  ) { }

  navigateToResultsPage() {
    this.router.navigate([RouteConstants.RESULTS], {queryParams: {searchTaskId: this.searchTask.id}});
  }

  navigateToAgentsPage() {
    this.router.navigate([RouteConstants.SEARCH_AGENTS], {queryParams: {searchTaskId: this.searchTask.id}});
  }
}
