import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { AgentLogService } from '../agent-log.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { RouteConstants } from 'src/app/routes';

@Component({
  selector: 'app-agent-logs-page',
  templateUrl: './agent-logs-page.component.html',
  styleUrls: ['./agent-logs-page.component.scss']
})
export class AgentLogsPageComponent implements OnInit {

  private queryParamSubscription: Subscription;

  private pageSize = 35;

  private agentUuid: string;

  private currentPage: number = 0;

  agentLogsPage: any;
  
  constructor(
    private agentLogService: AgentLogService,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private router: Router
  ) { }

  ngOnInit() {
    this.initQueryParamSubscription();
  }

  ngOnDestroy() {
    this.queryParamSubscription.unsubscribe();
  }

  private initQueryParamSubscription() {
    this.queryParamSubscription = this.route.queryParams.subscribe(queryParams => {
      this.agentUuid = queryParams.agentUuid;
      this.currentPage = queryParams.page;

      if (!this.currentPage) {
        throw new Error("Page number not provided.");
      }

      this.currentPage = parseInt(this.currentPage as any);

      if (this.agentUuid) {    
        this.loadAgentLogs(this.currentPage);
      } else {
        throw new Error("Agent uuid not provided.");
      }
    });
  }

  private loadAgentLogs(pageNumber) {
    this.spinner.show();
    this.agentLogService.findByAgentUuid(this.agentUuid, this.pageSize, pageNumber).subscribe((agentLogsPage: any) => {
      agentLogsPage.content = agentLogsPage.content.reverse()
      this.agentLogsPage = agentLogsPage;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
      throw new Error(error.message);
    });
  }

  isLastPage() {
    return this.agentLogsPage.number < this.agentLogsPage.totalPages - 1
  }

  isNotFirstPage() {
    return this.agentLogsPage.number > 0;
  }

  loadNextPage() {
    this.router.navigate([RouteConstants.AGENT_LOGS], 
      {queryParams: {agentUuid: this.agentUuid, page: this.currentPage + 1}});
  }

  loadPreviousPage() {
    this.router.navigate([RouteConstants.AGENT_LOGS], 
      {queryParams: {agentUuid: this.agentUuid, page: this.currentPage - 1}});
  }
}
