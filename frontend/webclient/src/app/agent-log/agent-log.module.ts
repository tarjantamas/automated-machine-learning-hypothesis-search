import { NgModule } from '@angular/core';

import { AgentLogRoutingModule } from './agent-log-routing.module';
import { SharedModule } from '../shared/shared.module';
import { AgentLogListComponent } from './agent-log-list/agent-log-list.component';
import { AllAgentLogCardComponent } from './agent-log-list/all-agent-log-card/all-agent-log-card.component';
import { AgentLogsPageComponent } from './agent-logs-page/agent-logs-page.component';


@NgModule({
  declarations: [
    AgentLogsPageComponent,
    AgentLogListComponent,    
    AllAgentLogCardComponent,
  ],
  imports: [
    SharedModule,
    AgentLogRoutingModule
  ]
})
export class AgentLogModule { }
