export class AgentLog {

  constructor(
    public id: number,
    public text: string,
    public agentUuid: string,
    public traceback: AgentLogTraceback
  ) {}
}

class AgentLogTraceback {
  
  constructor(
    public id: number,
    public text: string
  ) {}
}