import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteConstants } from '../routes';
import { AgentLogsPageComponent } from './agent-logs-page/agent-logs-page.component';

const routes: Routes = [
  { path: RouteConstants.AGENT_LOGS, component: AgentLogsPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgentLogRoutingModule { }
