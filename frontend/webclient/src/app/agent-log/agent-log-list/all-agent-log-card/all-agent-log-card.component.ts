import { Component, OnInit, Input, ElementRef } from '@angular/core';
import { AgentLog } from '../../agent-log.model';
import { Router } from '@angular/router';
import { RouteConstants } from 'src/app/routes';

@Component({
  selector: 'app-all-agent-log-card',
  templateUrl: './all-agent-log-card.component.html',
  styleUrls: ['./all-agent-log-card.component.scss']
})
export class AllAgentLogCardComponent {

  @Input()
  agentLog: AgentLog;

  showTraceback: boolean = false;
}
