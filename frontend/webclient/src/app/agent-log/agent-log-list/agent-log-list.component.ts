import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { AgentLogService } from '../agent-log.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { AgentLog } from '../agent-log.model';

@Component({
  selector: 'app-agent-log-list',
  templateUrl: './agent-log-list.component.html',
  styleUrls: ['./agent-log-list.component.scss']
})
export class AgentLogListComponent {

  logViewHeight = window.innerHeight - 180;

  @Input()
  agentLogs: AgentLog[];
}
