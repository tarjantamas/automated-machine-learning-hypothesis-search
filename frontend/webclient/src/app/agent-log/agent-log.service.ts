import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { AgentLog } from './agent-log.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AgentLogService {

  private agentLogsEndpoint = environment.agentLogsEndpoint;

  constructor(
    private http: HttpClient
  ) { }

  create(agentLog: AgentLog): Observable<any> {
    return this.http.post(this.agentLogsEndpoint, agentLog);
  }

  findAll(): Observable<any> {
    return this.http.get(this.agentLogsEndpoint);
  }

  findByAgentUuid(agentUuid: string, size: number, page: number) {
    return this.http.get(`${this.agentLogsEndpoint}?agent-uuid=${agentUuid}&page=${page}&size=${size}&sort=id,desc`);
  }
}
