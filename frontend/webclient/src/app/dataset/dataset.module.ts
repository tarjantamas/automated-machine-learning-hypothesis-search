import { NgModule } from '@angular/core';

import { DatasetRoutingModule } from './dataset-routing.module';
import { DatasetListComponent } from './dataset-list/dataset-list.component';
import { SharedModule } from '../shared/shared.module';
import { DatasetUploadComponent } from './dataset-upload/dataset-upload.component';
import { DatasetPageComponent } from './dataset-page/dataset-page.component';
import { DlDatasetCardComponent } from './dataset-list/dl-dataset-card/dl-dataset-card.component';


@NgModule({
  declarations: [
    DatasetListComponent, 
    DatasetUploadComponent, 
    DatasetPageComponent, 
    DlDatasetCardComponent
  ],
  imports: [
    SharedModule,
    DatasetRoutingModule
  ]
})
export class DatasetModule { }
