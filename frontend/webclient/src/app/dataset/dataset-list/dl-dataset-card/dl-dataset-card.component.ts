import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { DatasetListComponent } from '../dataset-list.component';
import { Dataset } from '../../dataset.model';
import { ContextMenuComponent } from 'src/app/shared/context-menu/context-menu.component';
import { Router } from '@angular/router';
import { RouteConstants } from 'src/app/routes';
import { environment } from 'src/environments/environment';
import { DatasetService } from '../../dataset.service';

@Component({
  selector: 'app-dl-dataset-card',
  templateUrl: './dl-dataset-card.component.html',
  styleUrls: ['./dl-dataset-card.component.scss']
})
export class DlDatasetCardComponent implements OnInit {

  @ViewChild(ContextMenuComponent, { static: true }) 
  contextMenu: ContextMenuComponent;

  @Input() 
  dataset: Dataset;

  @Input()
  color: string = 'white';

  constructor(
    public elementRef: ElementRef,
    private datasetService: DatasetService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  newSearchTask() {
    this.router.navigate([RouteConstants.HYPOTHESIS_SEARCH], 
      { queryParams: { datasetId: this.dataset.id } })
  }

  downloadDataset() {
    this.datasetService.donwloadDataset(this.dataset.id);
  }
}
