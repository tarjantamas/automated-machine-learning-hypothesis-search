import { Component, OnInit } from '@angular/core';
import { DatasetService } from '../dataset.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Dataset } from '../dataset.model';

@Component({
  selector: 'app-dataset-list',
  templateUrl: './dataset-list.component.html',
  styleUrls: ['./dataset-list.component.scss']
})
export class DatasetListComponent implements OnInit {

  datasetPage: any;

  cardColors = ['#9D2A24', '#358494', '#DC4539', '#87B7C2', '#F66C5E', '#EBA266'];

  constructor(
    private spinner: NgxSpinnerService,
    private datasetService: DatasetService
  ) { }

  ngOnInit() {
    this.spinner.show();
    this.datasetService.findAll().subscribe(page => {
      this.datasetPage = page;
      this.spinner.hide();
    });
  }

  insert(dataset: Dataset) {
    this.datasetPage.content.push(dataset);
  }

  getCardColor(cardIndex: number) {
    let colorIndex = cardIndex % this.cardColors.length;
    
    return this.cardColors[colorIndex];
  }

}