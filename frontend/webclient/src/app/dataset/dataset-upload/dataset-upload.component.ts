import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DatasetService } from '../dataset.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FileUploadData } from 'src/app/shared/file-upload/file-upload-data.model';
import { ToastService } from 'src/app/core/toast.service';

@Component({
  selector: 'app-dataset-upload',
  templateUrl: './dataset-upload.component.html',
  styleUrls: ['./dataset-upload.component.scss']
})
export class DatasetUploadComponent implements OnInit {

  @Output()
  uploadSuccess: EventEmitter<any> = new EventEmitter();

  datasetName: string;

  fileUploadData: FileUploadData;

  constructor(
    private datasetService: DatasetService,
    private toast: ToastService,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit() {
  }

  uploadDataset(fileUploadData: FileUploadData) {
    this.fileUploadData = fileUploadData;
    this.datasetName = this.fileUploadData.file.name;
  }

  datasetSelectionError(message: any) {
    throw new Error(message);
  }

  upload() {
    this.validateFileUpload();
    this.spinner.show();
    this.datasetService.uploadDataset(this.fileUploadData.file, this.datasetName).subscribe(result => {
      this.uploadSuccess.emit(result);
      this.spinner.hide();
      this.clearFields();
      this.toast.success("Dataset has been uploaded.");
    }, error => {
      this.spinner.hide();
      throw new Error(error.message);
    });
  }

  private clearFields() {
    this.datasetName = "";
    this.fileUploadData = null;
  }

  private validateFileUpload() {
    if (!this.fileUploadData || !this.datasetName) {
      throw new Error("You have to choose a dataset and give it a name.");
    }
  }
}
