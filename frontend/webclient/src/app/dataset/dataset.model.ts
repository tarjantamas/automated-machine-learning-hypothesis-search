export class Dataset {
  
  constructor(
    public id?: number,
    public name?: string,
    public signature?: string,
    public fileName?: string
  ) {}
}