import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Dataset } from './dataset.model';

@Injectable({
  providedIn: 'root'
})
export class DatasetService {
  
  datasetEndpoint = environment.datasetEndpoint;

  datasetsEndpoint = environment.datasetsEndpoint;

  constructor(
    private http: HttpClient
  ) { }

  uploadDataset(file, name: string): Observable<Dataset> {
    let formData: FormData = new FormData();

    formData.append("file", file);

    return this.http.post(`${this.datasetsEndpoint}/dataset-upload?name=${name}`, formData);
  }

  findAll() {
    return this.http.get(this.datasetsEndpoint);
  }

  donwloadDataset(datasetId: number) {
    window.open(`${this.datasetEndpoint}/download/${datasetId}`, '_blank');
  }

  findById(datasetId: any) {
    return this.http.get(`${this.datasetEndpoint}/${datasetId}`);
  }
}
