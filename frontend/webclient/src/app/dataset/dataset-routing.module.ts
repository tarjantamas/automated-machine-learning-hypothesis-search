import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteConstants } from '../routes';
import { DatasetPageComponent } from './dataset-page/dataset-page.component';


const routes: Routes = [
  { path: RouteConstants.DATASETS, component: DatasetPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DatasetRoutingModule { }
