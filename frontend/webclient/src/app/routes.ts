
export const RouteConstants = {
  HOME: "home",
  DATASETS: "datasets",
  HYPOTHESIS_SEARCH: "hypothesis-search",
  SEARCH_AGENTS: "search-agents",
  AGENT_LOGS: "agent-logs",
  SEARCH_TASKS: "search-tasks",
  RESULTS: "results",
  DOCUMENTATION: "documentation",
  SEARCH_TASK_DOCUMENTATION: "documentation/search-task",
  PREPROCESSOR_DOCUMENTATION: "documentation/preprocessor",
};