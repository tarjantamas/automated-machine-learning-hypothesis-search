import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ResultService {

  private resultsEndpoint = environment.resultsEndpoint;

  constructor(
    private http: HttpClient
  ) { }

  findBySearchTaskId(searchTaskId: number, sortColumn?, sortOrder?) {
    let sort = 'sort=mean,desc';
    if (sortColumn) {
      sort = `sort=${sortColumn},${sortOrder}`;
    }
    return this.http.get(`${this.resultsEndpoint}?search-task-id=${searchTaskId}&size=100&${sort}`);
  }
}
