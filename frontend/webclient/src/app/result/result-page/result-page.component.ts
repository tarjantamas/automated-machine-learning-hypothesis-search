import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { ResultService } from '../result.service';
import { SearchTaskService } from 'src/app/hypothesis/search-task.service';
import { SearchTask } from 'src/app/hypothesis/search-task.model';

@Component({
  selector: 'app-result-page',
  templateUrl: './result-page.component.html',
  styleUrls: ['./result-page.component.scss']
})
export class ResultPageComponent implements OnInit, OnDestroy {

  private queryParamSubscription: Subscription;

  resultsPage: any;

  private searchTaskId;

  searchDurationMs: number;

  searchStartTime: Date;

  constructor(
    private resultService: ResultService,
    private searchTaskService: SearchTaskService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.initQueryParamSubscription();
  }

  ngOnDestroy() {
    this.queryParamSubscription.unsubscribe();
  }

  private initQueryParamSubscription() {
    this.queryParamSubscription = this.route.queryParams.subscribe(queryParams => {
      this.searchTaskId = queryParams.searchTaskId;
      
      if (this.searchTaskId) {
        this.loadResults(this.searchTaskId);
      } else {
        throw new Error("Search task id not provided.");
      }
    });
  }

  private loadResults(searchTaskId: number, sortColumn?, sortOrder?) {
    this.spinner.show();

    this.resultService.findBySearchTaskId(searchTaskId, sortColumn, sortOrder).subscribe(resultsPage => {
      this.resultsPage = resultsPage;
      this.searchTaskService.findById(searchTaskId).subscribe(searchTask => {
        let task = searchTask as SearchTask;
        this.searchStartTime = new Date(task.searchStartTimeMs);

        if (task.searchStartTimeMs && task.searchEndTimeMs) {
          this.searchDurationMs = task.searchEndTimeMs - task.searchStartTimeMs;
        }

        this.spinner.hide(); 
      }, error => {
        this.spinner.hide();
        throw new Error(error.message);
      });
    }, error => {
      this.spinner.hide();
      throw new Error(error.message);
    });
  }

  sortChanged(sort) {
    this.loadResults(this.searchTaskId, sort.column, sort.order);
  }
}
