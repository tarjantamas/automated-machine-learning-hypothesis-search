import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Result } from '../result.model';
import { ToastService } from 'src/app/core/toast.service';

@Component({
  selector: 'app-result-list',
  templateUrl: './result-list.component.html',
  styleUrls: ['./result-list.component.scss']
})
export class ResultListComponent {

  @Input()
  results: Result[];

  @Output()
  sortChanged = new EventEmitter();

  currentSort = {}

  constructor(
    public toast: ToastService
  ) {}

  changeSort(col) {
    if (!this.currentSort[col]) {
      this.currentSort[col] = 'asc';
    } else if (this.currentSort[col] == 'asc') {
      this.currentSort[col] = 'desc';
    } else {
      this.currentSort[col] = 'asc';
    }
    this.sortChanged.emit({
      column: col,
      order: this.currentSort[col]
    });
  }
}
