import { Component, Input } from '@angular/core';
import { Result } from '../../result.model';

@Component({
  selector: 'app-rl-result-card',
  templateUrl: './rl-result-card.component.html',
  styleUrls: ['./rl-result-card.component.scss']
})
export class RlResultCardComponent {

  @Input()
  result: Result;
}
