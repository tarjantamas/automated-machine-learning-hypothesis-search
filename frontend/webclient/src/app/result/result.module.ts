import { NgModule } from '@angular/core';

import { ResultRoutingModule } from './result-routing.module';
import { ResultListComponent } from './result-list/result-list.component';
import { ResultPageComponent } from './result-page/result-page.component';
import { RlResultCardComponent } from './result-list/rl-result-card/rl-result-card.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    ResultListComponent, 
    ResultPageComponent, 
    RlResultCardComponent],
  imports: [
    SharedModule,
    ResultRoutingModule
  ]
})
export class ResultModule { }
