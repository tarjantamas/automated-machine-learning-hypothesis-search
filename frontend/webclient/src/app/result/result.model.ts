export class Result {

  constructor(
    public id: number,
    public hypothesisIndex: number,
    public min: number,
    public max: number,
    public mean: number,
    public median: number,
    public std: number,
    public agentUuid: string,
    public searchTaskId: number,
    public hypothesis: string
  ) { }
}