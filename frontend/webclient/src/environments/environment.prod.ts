const ROOT_ENDPOINT = "http://167.71.32.67:45569/api/v1";

export const environment = {
  production: false,
  datasetEndpoint: ROOT_ENDPOINT + "/dataset",
  datasetsEndpoint: ROOT_ENDPOINT + "/datasets",
  searchTaskEndpoint: ROOT_ENDPOINT + "/search-task",
  searchTasksEndpoint: ROOT_ENDPOINT + "/search-tasks",
  resultsEndpoint: ROOT_ENDPOINT + "/results",
  agentsEndpoint: ROOT_ENDPOINT + "/agents",
  agentLogsEndpoint: ROOT_ENDPOINT + "/agent-logs"
};