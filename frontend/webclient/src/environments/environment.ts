// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const ROOT_ENDPOINT = "http://167.71.32.67:45569/api/v1";

export const environment = {
  production: false,
  datasetEndpoint: ROOT_ENDPOINT + "/dataset",
  datasetsEndpoint: ROOT_ENDPOINT + "/datasets",
  searchTaskEndpoint: ROOT_ENDPOINT + "/search-task",
  searchTasksEndpoint: ROOT_ENDPOINT + "/search-tasks",
  resultsEndpoint: ROOT_ENDPOINT + "/results",
  agentsEndpoint: ROOT_ENDPOINT + "/agents",
  agentLogsEndpoint: ROOT_ENDPOINT + "/agent-logs"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
