def fence(column):
  q1 = column.quantile(0.25)
  q2 = column.quantile(0.75)

  iqr = q2 - q1

  fence_low = q1 - 1.5 * iqr
  fence_high = q2 + 1.5 * iqr

  return fence_low, fence_high


import pandas as pd

data = pd.read_csv('../example/adult.csv')

for column_name in data.columns:
  try:
    column = data[column_name]
    fence_low, fence_high = fence(column)
    index = (column >= fence_low) & (column <= fence_high)
    data = data.loc[index]
  except:
    pass

print(data)


