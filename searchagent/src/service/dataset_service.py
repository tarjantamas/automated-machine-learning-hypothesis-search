import os
import requests
from service.service import Service
import config.agent_configuration as agent_config


class DatasetService(Service):
  def __init__(self, logger):
    super().__init__(logger)
    self.dataset_endpoint = agent_config.dataset_endpoint
  
  def download(self, dataset_id):
    endpoint = self.get_endpoint(self.dataset_endpoint, 'download', str(dataset_id))
    response = requests.get(endpoint, allow_redirects=True)
    dataset_path = self._get_dataset_path_from_id(dataset_id)
    with open(dataset_path, 'wb') as outfile:
      outfile.write(response.content)
    
    return dataset_path
  
  def get_dataset_path(self, dataset_id):
    dataset_path = self._get_dataset_path_from_id(dataset_id)
    if not os.path.exists(dataset_path):
      return self.download(dataset_id)

    return dataset_path

  def _get_dataset_path_from_id(self, dataset_id):
    if not os.path.exists(agent_config.dataset_store_path):
      os.makedirs(agent_config.dataset_store_path)

    return agent_config.dataset_store_path + '/' + str(dataset_id)