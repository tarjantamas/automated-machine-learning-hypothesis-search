from service.service import Service
import config.agent_configuration as agent_config
import requests
import os


class TaskFrameService(Service):
  def __init__(self, agent_service, logger):
    super().__init__(logger)
    self.agent_service = agent_service
    self.task_frames_endpoint = agent_config.task_frames_endpoint
  
  def apply_for_task_frame(self, agent_uuid):
    self.agent_service.update_status(agent_uuid, 'Applying for task')

    endpoint = self.get_endpoint(self.task_frames_endpoint, 'apply-for-task-frame')

    self.log('Applying for task frame using the following endpoint: {}'.format(endpoint))


    while True:
      result = requests.get(endpoint, params={'agent-uuid': agent_uuid}, timeout=20).json()

      if 'message' in result:
        message = result['message']
        if message == 'timeout':
          print('Long polling tick.')
        else:
          try:
            self.log('Removing old agent info.')
            os.remove(agent_config.agent_info_path)
          except:
            raise Exception(result['message'])
          raise Exception("Existing agent info no longer valid.")
      else:
        break
  

    if 'message' in result:
      self.log(result['message'])

    return result
