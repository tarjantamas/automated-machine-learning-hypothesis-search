import config.agent_configuration as agent_config
import requests
from service.service import Service


class ResultService(Service):
  def __init__(self, logger):
    super().__init__(logger)
    self.results_endpoint = agent_config.results_endpoint

  def create(self, result):
    return requests.post(self.results_endpoint, json=result).json()
