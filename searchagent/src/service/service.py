class Service:
  def __init__(self, logger):
    self.logger = logger.get_logger(self.__class__.__name__)

  def get_endpoint(self, *endpoint, **params):
    result = "{}".format('/'.join(endpoint))

    if params:
      param_string = '?'
      for key, value in params.items():
        param_string += "{}={}&".format(key, value)
      result += param_string[:-1]
    
    return result
    
  def log(self, text):
    self.logger.log(text)