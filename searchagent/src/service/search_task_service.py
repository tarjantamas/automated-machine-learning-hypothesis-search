import os
import requests
from service.service import Service
import config.agent_configuration as agent_config


class SearchTaskService(Service):
  def __init__(self, logger):
    super().__init__(logger)
    self.search_task_endpoint = agent_config.search_task_endpoint
  
  def download(self, search_task_id):
    endpoint = self.get_endpoint(self.search_task_endpoint, 'download', str(search_task_id))
    response = requests.get(endpoint, allow_redirects=True)
    search_task_path = self._get_search_task_path_from_id(search_task_id)
    with open(search_task_path, 'wb') as outfile:
      outfile.write(response.content)
    
    return search_task_path.replace('/', '.').replace('.py', '')

  def get_search_task_path(self, search_task_id):
    search_task_path = self._get_search_task_path_from_id(search_task_id)
    if not os.path.exists(search_task_path):
      return self.download(search_task_id)

    return search_task_path.replace('/', '.').replace('.py', '')

  def _get_search_task_path_from_id(self, search_task_id):
    if not os.path.exists(agent_config.search_tasks_path):
      os.makedirs(agent_config.search_tasks_path)

    return "{}/search_task_{}.py".format(agent_config.search_tasks_path, str(search_task_id))
