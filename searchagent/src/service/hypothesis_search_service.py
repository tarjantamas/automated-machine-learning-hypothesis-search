from service.service import Service
from model.choice_space import dict_to_object
import multiprocessing as mp
from model.param_space import ParamSpace
from jsonutil.object_filter import object_filter
import copy
import random


random.seed(42)


class HypothesisSearchService(Service):
  def __init__(self, result_service, agent_service, logger):
    super().__init__(logger)
    self.result_service = result_service
    self.agent_service = agent_service

  def search(self, search_resources):
    self.logger.log("Starting hypothesis search.")
    self.agent_service.update_status(search_resources.agent_dto['uuid'], 'Performing hypothesis search')

    evaluator = Evaluator(self.result_service, search_resources, self.logger)

    task_frame = search_resources.task_frame

    start, end = task_frame['searchStart'], task_frame['searchEnd']
    search_space_size = end - start
    search_range = random.sample(range(start, end), k=search_space_size)

    self.log("Testing hypothesies from range [{}, {}). Search task id: [{}]".format(start, end, search_resources.search_task_info['id']))
    self.agent_service.update_status(search_resources.agent_dto['uuid'], "Testing hypothesies from range [{}, {}). Search task id: [{}]".format(start, end, search_resources.search_task_info['id']))
    
    pool = mp.Pool(1)
    pool.map(evaluator.evaluate, search_range)
    pool.close()
    pool.join()

    self.log("Task frame done.")


class Evaluator(Service):
  def __init__(self, result_service, search_resources, logger):
    super().__init__(logger)
    self.result_service = result_service
    self.search_task_info = search_resources.search_task_info
    self.hypothesis_space = search_resources.search_task.hypothesis_space
    self.hypothesis_pipeline = search_resources.search_task.hypothesis_pipeline
    self.optimizer_class = search_resources.search_task.param_optimizer
    self.dataset_path = search_resources.dataset_path
    self.agent_dto = search_resources.agent_dto
    self.param_manager = ParamManager()

  def evaluate(self, hypothesis_index):
    hypothesis = dict_to_object(self.hypothesis_space[hypothesis_index])

    self.log('Optimizing hyper-parameters for hypothesis [{}].'.format(hypothesis_index))
    result = self._get_optimized_result(hypothesis)
    
    result = self._add_result_meta_data(result, hypothesis_index, self.search_task_info, self.agent_dto)
    self.result_service.create(result)
  
  def _get_optimized_result(self, hypothesis):
    self.param_manager.add_params_from_hypothesis(hypothesis)
    param_spaces = self.param_manager.get_param_spaces()

    if len(param_spaces) > 0:
      blackbox = self._get_blackbox(hypothesis, self.hypothesis_pipeline, param_spaces)
      optimizer = self.optimizer_class(param_spaces, blackbox)
      optimizer.optimize()
      best_result = optimizer.best()

      ParamSpace.discretize_discrete_params(best_result['params'], param_spaces)
      self.param_manager.inject_params_into_hypothesis(hypothesis, best_result['params'])

    result = self.hypothesis_pipeline(self.dataset_path, hypothesis)
    result.hypothesis = str(hypothesis)

    return result

  def _get_blackbox(self, hypothesis, pipeline, param_spaces):
    def blackbox(**params):
      ParamSpace.discretize_discrete_params(params, param_spaces)

      self.param_manager.inject_params_into_hypothesis(hypothesis, params)

      result = pipeline(self.dataset_path, hypothesis)

      return result.mean

    return blackbox

  def _add_result_meta_data(self, result, hypothesis_index, search_task_info, agent_dto):
    result = result.__dict__
    result['hypothesisIndex'] = hypothesis_index
    result['searchTaskId'] = search_task_info['id']
    result['agentUuid'] = agent_dto['uuid']

    return result


class ParamManager:
  SEPARATOR = "__SEP__"

  def __init__(self):
    self.params = {}
  
  def add_param_space(self, param_path, param_space):
    self.params[param_path] = param_space

  def get_param_spaces(self):
    return self.params

  def add_params_from_hypothesis(self, hypothesis):
    for path, _, param_space in object_filter(hypothesis, separator=ParamManager.SEPARATOR, stop_type=ParamSpace):
      self.add_param_space(path, param_space)

  def inject_params_into_hypothesis(self, hypothesis, params):
    for group, value in params.items():
      keys = group.split(ParamManager.SEPARATOR)
      
      current_obj = hypothesis
      for key in keys[:-1]:
        current_obj = current_obj.__dict__[key]

      current_obj.__dict__[keys[-1]] = value
