from service.service import Service
import multiprocessing as mp


class SearchResourceService(Service):
  def __init__(self, task_frame_service, dataset_service, search_task_service, agent_service, agent_log_service, logger):
    super().__init__(logger)
    self.task_frame_service = task_frame_service
    self.dataset_service = dataset_service
    self.search_task_service = search_task_service
    self.agent_service = agent_service
    self.agent_log_service = agent_log_service

  def get_search_resources(self):
    search_resources = SearchResources()

    search_resources.agent_dto = self.agent_service.get_agent_dto(mp.cpu_count())
    self.agent_log_service.agent_uuid = search_resources.agent_dto['uuid']

    search_resources.task_frame = self.task_frame_service.apply_for_task_frame(search_resources.agent_dto['uuid'])
    search_resources.search_task_info = search_resources.task_frame['searchTask']

    self.log("Aquiring dataset path.")
    self.agent_service.update_status(search_resources.agent_dto['uuid'], 'Aquiring dataset')

    search_resources.dataset_path = self.dataset_service.get_dataset_path(search_resources.search_task_info['datasetId'])

    self.log("Aquiring search task path.")
    self.agent_service.update_status(search_resources.agent_dto['uuid'], 'Aquiring search task source')

    search_task_file_path = self.search_task_service.get_search_task_path(search_resources.search_task_info['id'])
    search_resources.search_task = __import__(search_task_file_path, fromlist=[''])

    return search_resources


class SearchResources:
  def __init__(self):
    self.search_task = None
    self.task_frame = None
    self.search_task_info = None
    self.dataset_path = None
    self.agent_dto = None

