import os
import requests
import multiprocessing
from service.service import Service
from jsonutil.mapper import JsonMapper
import config.agent_configuration as agent_config


class AgentService(Service):
  def __init__(self, logger):
    super().__init__(logger)
    self.agents_endpoint = agent_config.agents_endpoint
    self.agent_endpoint = agent_config.agent_endpoint
    self.agent_uuid = ""

  def update_status(self, agent_uuid=None, status=""):
    if agent_uuid is not None:
      self.agent_uuid = agent_uuid
    else:
      agent_uuid = self.agent_uuid

    endpoint = self.get_endpoint(self.agent_endpoint, 'update-status')

    return requests.put(endpoint, json={'uuid': agent_uuid, 'status': status})

  def register(self, cpu_count):
    endpoint = self.get_endpoint(self.agents_endpoint, 'register')

    return requests.post(endpoint, json={'cpuCount': cpu_count}).json()

  def get_agent_dto(self, cpu_count):
    self.log("Checking if agent info available.")
    if os.path.exists(agent_config.agent_info_path):
      self.log("Agent info is available, loading agent info...")
      with open(agent_config.agent_info_path, 'r') as infile:
        return JsonMapper.from_json(infile.read())

    self.log("Agent info is not available, trying to register agent info.")
    agent_dto = self.register(1) # multiprocessing.cpu_count()

    if 'message' in agent_dto:
      self.log(agent_dto['message'])
      raise Exception(agent_dto['message'])
    
    self.log("Agent regisetered, saving agent info...")
    with open(agent_config.agent_info_path, 'w') as outfile:
      self.log("Agent info saved.")
      outfile.write(JsonMapper.to_json(agent_dto))

    return agent_dto