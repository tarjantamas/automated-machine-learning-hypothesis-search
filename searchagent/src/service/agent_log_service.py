from service.service import Service
import config.agent_configuration as agent_config
import requests


class AgentLogService(Service):
  def __init__(self):
    self.agent_logs_endpoint = agent_config.agent_logs_endpoint
    self.agent_uuid = None

  def create(self, log):
    if self.agent_uuid is None:
      raise Exception("Can't create log, agent uuid not set in {}.".format(self.__class__.__name__))

    log['agentUuid'] = self.agent_uuid

    return requests.post(self.agent_logs_endpoint, json=log)
