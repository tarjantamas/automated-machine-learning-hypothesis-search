def object_filter(obj, separator='', stop_type=None, _current_path='', _key=''):
  if isinstance(obj, stop_type):
    yield _current_path, _key, obj
    return
  
  current_separator = ''
  if _current_path != '':
    current_separator = separator
    
  items = None
  try:
    if isinstance(obj, dict):
      items = obj.items()
    elif isinstance(obj, object):
      items = obj.__dict__.items()
  except:
    pass

  if items is not None:
    for key, value in items:
      yield from object_filter(value, _current_path=_current_path + current_separator + key, separator=separator, _key=key, stop_type=stop_type)