import json

class JsonMapper:
  
  @staticmethod
  def from_json(json_string):
    return json.loads(json_string)

  @staticmethod
  def to_json(obj):
    return json.dumps(obj)