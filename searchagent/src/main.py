import sys
import os
from traceback import format_exc
from service.agent_service import AgentService
from service.task_frame_service import TaskFrameService
from service.dataset_service import DatasetService
from service.search_task_service import SearchTaskService
from service.result_service import ResultService
from service.agent_log_service import AgentLogService
from service.hypothesis_search_service import HypothesisSearchService
from service.search_resource_service import SearchResourceService
from logger.logger import Logger


class Main:
  def __init__(self):
    self.agent_log_service = AgentLogService()
    self.logger = Logger(self.agent_log_service).get_logger(self.__class__.__name__)

  def main(self):
    logger = self.logger

    agent_service = AgentService(logger)
    task_frame_service = TaskFrameService(agent_service, logger)
    dataset_service = DatasetService(logger)
    search_task_service = SearchTaskService(logger)
    result_service = ResultService(logger)

    hypothesis_search_service = HypothesisSearchService(result_service, agent_service, logger)
    search_resource_service = SearchResourceService(task_frame_service, dataset_service, search_task_service, agent_service, self.agent_log_service, logger)

    while True:
      try:
        search_resources = search_resource_service.get_search_resources()
        hypothesis_search_service.search(search_resources)
      except BaseException as e:
        if isinstance(e, KeyboardInterrupt):
          logger.log("Stopped by user.")
          
          agent_service.update_status(agent_uuid="4e7b5f3f-c861-49ca-acb9-f821400bcb93", status="Stopped")
          try:
            sys.exit(0)
          except SystemExit:
            os._exit(0)
        logger.exception(repr(e), format_exc())


if __name__ == '__main__':
  if len(sys.argv) > 1 and sys.argv[1] == '--size':
    from default import search_task as st
    print(len(st.hypothesis_space))
  elif len(sys.argv) > 2 and sys.argv[1] == '--i':
    from default import search_task as st
    print(st.hypothesis_space[int(sys.argv[2])])
  else:
    app = Main()
    app.main()
