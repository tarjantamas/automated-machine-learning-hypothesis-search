import pandas as pd

from sklearn.ensemble import VotingRegressor, \
  AdaBoostRegressor, GradientBoostingRegressor, RandomForestRegressor
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import RepeatedKFold
from sklearn.neighbors import KNeighborsRegressor
from sklearn.tree import DecisionTreeRegressor

from data_processor.meta import ColumnMetaData
from data_processor.preprocessor import OrdinalEncoder, MinMaxScaler, StandardScaler, CategoricalImputer, \
  NumericImputer, TukeyOutlierRemover

from model.param_space import ParamSpace
from model.result import Result
from model.hypothesis_space import HypothesisSpaceBuilder

from optimizer.bayesian import BayesianOptimizer


def voting_constructor(**p):
  weight = p['weight']
  rfr = RandomForestRegressor(n_estimators=p['rfr_n_estimators'], random_state=p['random_state'],
                              max_depth=p['rfr_max_depth'])
  gbr = GradientBoostingRegressor(n_estimators=p['gbr_n_estimators'], subsample=p['gbr_subsample'],
                                  max_depth=p['gbr_max_depth'], random_state=p['random_state'])
  voting_regressor = VotingRegressor(estimators=[('rfr', rfr), ('gbr', gbr)], weights=[weight, 10 - weight])

  return voting_regressor


def get_x_y(data, response_column_name):
  return data.drop(columns=[response_column_name]), data[response_column_name]


hypothesis_space = HypothesisSpaceBuilder() \
  .group(name='categorical_imputer') \
    .constructor_option(constructor=CategoricalImputer) \
  .group(name='numeric_imputer') \
    .constructor_option(constructor=NumericImputer) \
  .group(name='categorical_encoder') \
    .constructor_option(constructor=OrdinalEncoder) \
  .group(name='outlier_remover') \
    .constructor_option(constructor=TukeyOutlierRemover) \
      .param(name='k', values=ParamSpace(1.5, 3)) \
  .group(name='scaler') \
    .constructor_option(constructor=MinMaxScaler) \
    .constructor_option(constructor=StandardScaler) \
  .group(name='model') \
    .constructor_option(constructor=DecisionTreeRegressor) \
      .param(name='max_depth', values=ParamSpace(3, 30, is_discrete=True)) \
      .param(name='max_features', values=ParamSpace(0.2, 1)) \
      .param(name='random_state', values=42) \
    .constructor_option(constructor=AdaBoostRegressor) \
      .param(name='n_estimators', values=ParamSpace(10, 500, is_discrete=True)) \
      .param(name='random_state', values=42) \
    .constructor_option(constructor=RandomForestRegressor) \
      .param(name='n_estimators', values=[ParamSpace(5, 30, is_discrete=True), ParamSpace(100, 200, is_discrete=True), ParamSpace(400, 500, is_discrete=True)]) \
      .param(name='max_depth', values=ParamSpace(3, 30, is_discrete=True)) \
      .param(name='random_state', values=42) \
      .param(name='max_features', values=ParamSpace(0.5, 1)) \
    .constructor_option(constructor=voting_constructor) \
      .param(name='rfr_n_estimators', values=ParamSpace(100, 500, is_discrete=True)) \
      .param(name='rfr_max_depth', values=ParamSpace(3, 15, is_discrete=True)) \
      .param(name='gbr_n_estimators', values=ParamSpace(100, 500, is_discrete=True)) \
      .param(name='gbr_subsample', values=ParamSpace(0.2, 1)) \
      .param(name='gbr_max_depth', values=ParamSpace(3, 15, is_discrete=True)) \
      .param(name='random_state', values=42) \
      .param(name='weight', values=ParamSpace(1, 5)) \
    .constructor_option(constructor=KNeighborsRegressor) \
      .param(name='n_neighbors', values=ParamSpace(3, 15, is_discrete=True)) \
  .build()


class Optimizer(BayesianOptimizer):
  def __init__(self, params_to_optimize, blackbox):
    def blackbox_wrapper(*args, **kwargs):
      return -blackbox(*args, **kwargs)
    super().__init__(params_to_optimize, blackbox_wrapper, init_points=2, n_iter=10)


param_optimizer = Optimizer


def hypothesis_pipeline(dataset_path, hypothesis):
  scores = []

  data = pd.read_csv(dataset_path, index_col=False)

  target = 'Grade'
  data_x, data_y = get_x_y(data, target)

  column_meta_data = ColumnMetaData.from_data(data_x)

  categorical_encoder = construct(hypothesis.categorical_encoder)
  categorical_columns = column_meta_data.categorical_columns + column_meta_data.binary_columns
  categorical_encoder.fit(data_x, categorical_columns)

  data_splitter = RepeatedKFold(n_splits=4, n_repeats=1, random_state=42)
  for train_index, test_index in data_splitter.split(data_x, data_y):
    train_x, train_y = data_x.loc[train_index], data_y.loc[train_index]
    test_x, test_y = data_x.loc[test_index], data_y.loc[test_index]

    categorical_imputer = construct(hypothesis.categorical_imputer)
    numeric_imputer = construct(hypothesis.numeric_imputer)

    train_x = categorical_imputer.fit_transform(train_x, column_meta_data.categorical_columns)
    train_x = numeric_imputer.fit_transform(train_x, column_meta_data.numeric_columns)

    test_x = categorical_imputer.transform(test_x, column_meta_data.categorical_columns)
    test_x = numeric_imputer.transform(test_x, column_meta_data.numeric_columns)

    train_x = categorical_encoder.transform(train_x, categorical_columns)
    test_x = categorical_encoder.transform(test_x, categorical_columns)

    outlier_remover = construct(hypothesis.outlier_remover)
    train_x, train_y = outlier_remover.transform(train_x, train_y, column_meta_data.numeric_columns)

    scaler = construct(hypothesis.scaler)
    train_x = scaler.fit_transform(train_x, column_meta_data.numeric_columns + column_meta_data.categorical_columns + column_meta_data.binary_columns)
    test_x = scaler.transform(test_x, column_meta_data.numeric_columns + column_meta_data.categorical_columns + column_meta_data.binary_columns)

    model = construct(hypothesis.model)

    model.fit(train_x, train_y)

    pred_y = model.predict(test_x)

    scores.append(mean_squared_error(test_y, pred_y))

  return Result(scores)


def construct(pack):
  return pack.constructor(**pack.params.__dict__)
