hypothesis_space = HypothesisSpaceBuilder() \
  .group(name='target_encoder')\
    .constructor_option(constructor=BinaryEncoder) \
      .param(name='p1', values=ParamSpace(10, 20, is_discrete=True)) \
  .group(name='model') \
    .constructor_option(constructor=BaggingClassifier) \
      .param(name='n_estimators', values=ParamSpace(10, 500, is_discrete=True)) \
      .param(name='max_features', values=ParamSpace(0.2, 1)) \
    .constructor_option(constructor=DecisionTreeClassifier) \
      .param(name='criterion', values=['gini', 'entropy']) \
      .param(name='max_depth', values=ParamSpace(3, 30, is_discrete=True)) \
  .build()

from model.choice_space import Choice, FixedChoice, ChoiceSpace
from model.param_space import ParamSpace

hypothesis_space = ChoiceSpace(choices=[
  Choice(name='target_encoder', options=[
    ChoiceSpace(choices=[
      FixedChoice(name='constructor', value=BinaryEncoder),
      ChoiceSpace(name='params', choices=[
        FixedChoice(name='p1', value=ParamSpace(10, 20, is_discrete=True))
      ])
    ])
  ]),
  Choice(name='model', options=[
    ChoiceSpace(choices=[
      FixedChoice(name='constructor', value=BaggingClassifier),
      ChoiceSpace(name='params', choices=[
        FixedChoice(name='n_estimators', value=ParamSpace(10, 500, is_discrete=True)),
        FixedChoice(name='max_features', value=ParamSpace(0.2, 1))
      ])
    ]),
    ChoiceSpace(choices=[
      FixedChoice(name='constructor', value=DecisionTreeClassifier),
      ChoiceSpace(name='params', choices=[
        Choice(name='criterion', options=['gini', 'entropy']),
        FixedChoice(name='max_depth', value=ParamSpace(3, 30, is_discrete=True))
      ])
    ])
  ])
])

{
  "target_encoder": {
    "constructor": BinaryEncoder,
    "params": {
      "p1": ParamSpace(10, 500, is_discrete=True)
    }
  },
  "model": {
    "constructor": DecisionTreeClassifier,
    "params": {
      "criterion": "gini",
      "max_depth": ParamSpace(3, 30, is_discrete=True)
    }
  }
}