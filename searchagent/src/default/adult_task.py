import pandas as pd

from sklearn.decomposition import PCA
from sklearn.ensemble import AdaBoostClassifier, GradientBoostingClassifier, RandomForestClassifier, \
  BaggingClassifier, ExtraTreesClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import RepeatedStratifiedKFold, KFold
from sklearn.tree import DecisionTreeClassifier
from xgboost import XGBClassifier

from data_processor.meta import ColumnMetaData
from data_processor.preprocessor import BinaryEncoder, OrdinalEncoder, MinMaxScaler, StandardScaler, CategoricalImputer, \
  NumericImputer, TukeyOutlierRemover, OneHotEncoder

from model.param_space import ParamSpace
from model.result import Result
from model.hypothesis_space import HypothesisSpaceBuilder

from optimizer.bayesian import BayesianOptimizer


def get_x_y(data, response_column_name):
  return data.drop(columns=[response_column_name]), data[response_column_name]


class NoOperationFitTransformerX:
  def transform(self, data_x):
    return data_x

  def fit(self, data_x):
    return data_x

  def fit_transform(self, data_x):
    return data_x


class NoOperationFitTransformerXY:
  def transform(self, data_x, data_y, *params):
    return data_x, data_y

  def fit(self, data_x, data_y, *params):
    return data_x, data_y

  def fit_transform(self, data_x, data_y, *params):
    return data_x, data_y


hypothesis_space = HypothesisSpaceBuilder() \
  .group(name='target_encoder')\
    .constructor_option(constructor=BinaryEncoder) \
  .group(name='categorical_imputer') \
    .constructor_option(constructor=CategoricalImputer) \
  .group(name='numeric_imputer') \
    .constructor_option(constructor=NumericImputer) \
  .group(name='categorical_encoder') \
    .constructor_option(constructor=OneHotEncoder) \
    .constructor_option(constructor=OrdinalEncoder) \
  .group(name='outlier_remover') \
    .constructor_option(constructor=NoOperationFitTransformerXY) \
    .constructor_option(constructor=TukeyOutlierRemover) \
      .param(name='k', values=ParamSpace(1.5, 3)) \
  .group(name='scaler') \
    .constructor_option(constructor=MinMaxScaler) \
    .constructor_option(constructor=StandardScaler) \
  .group(name='pca') \
    .constructor_option(constructor=NoOperationFitTransformerX) \
    .constructor_option(constructor=PCA) \
      .param(name='svd_solver', values='auto') \
      .param(name='n_components', values=ParamSpace(5, 14, is_discrete=True)) \
      .param(name='random_state', values=42) \
  .group(name='model') \
    .constructor_option(constructor=ExtraTreesClassifier) \
      .param(name='n_estimators', values=ParamSpace(50, 110, is_discrete=True)) \
      .param(name='min_samples_split', values=ParamSpace(10, 20, is_discrete=True)) \
      .param(name='max_features', values=ParamSpace(0.5, 0.8)) \
      .param(name='criterion', values='gini') \
      .param(name='bootstrap', values=True) \
      .param(name='random_state', values=42) \
    .constructor_option(constructor=BaggingClassifier) \
      .param(name='n_estimators', values=ParamSpace(10, 200, is_discrete=True)) \
      .param(name='max_features', values=ParamSpace(0.5, 1)) \
      .param(name='max_samples', values=ParamSpace(0.5, 1)) \
      .param(name='random_state', values=42) \
    .constructor_option(constructor=DecisionTreeClassifier) \
      .param(name='criterion', values=['gini', 'entropy']) \
      .param(name='max_depth', values=ParamSpace(2, 30, is_discrete=True)) \
      .param(name='random_state', values=42) \
    .constructor_option(constructor=AdaBoostClassifier) \
      .param(name='n_estimators', values=ParamSpace(10, 200, is_discrete=True)) \
      .param(name='random_state', values=42) \
    .constructor_option(constructor=GradientBoostingClassifier) \
      .param(name='n_estimators', values=ParamSpace(10, 200, is_discrete=True)) \
      .param(name='max_depth', values=ParamSpace(2, 30, is_discrete=True)) \
      .param(name='random_state', values=42) \
    .constructor_option(constructor=RandomForestClassifier) \
      .param(name='n_estimators', values=[ParamSpace(100, 300, is_discrete=True)]) \
      .param(name='criterion', values=['gini', 'entropy']) \
      .param(name='max_depth', values=ParamSpace(2, 30, is_discrete=True)) \
      .param(name='random_state', values=42) \
      .param(name='max_features', values=ParamSpace(0.5, 1)) \
      .param(name='min_samples_leaf', values=ParamSpace(80, 150, is_discrete=True)) \
    .constructor_option(constructor=RandomForestClassifier) \
      .param(name='n_estimators', values=[ParamSpace(10, 200, is_discrete=True)]) \
      .param(name='criterion', values=['gini', 'entropy']) \
      .param(name='max_depth', values=ParamSpace(3, 30, is_discrete=True)) \
      .param(name='random_state', values=42) \
      .param(name='max_features', values=ParamSpace(0.5, 1)) \
    .constructor_option(constructor=XGBClassifier) \
      .param(name='random_state', values=42) \
      .param(name='verbosity', values=0) \
      .param(name='max_depth', values=ParamSpace(2, 30, is_discrete=True)) \
      .param(name='n_estimators', values=ParamSpace(50, 200, is_discrete=True)) \
      .param(name='subsample', values=ParamSpace(0.5, 1)) \
  .build()


class Optimizer(BayesianOptimizer):
  def __init__(self, params_to_optimize, blackbox):
    super().__init__(params_to_optimize, blackbox, init_points=2, n_iter=30)

    
param_optimizer = Optimizer


def hypothesis_pipeline(dataset_path, hypothesis):
  scores = []

  data = pd.read_csv(dataset_path, index_col=False)

  print(data['target'].unique())

  target_encoder = construct(hypothesis.target_encoder)
  data = target_encoder.fit_transform(data, ['target'])


  data_x, data_y = get_x_y(data, 'target')

  column_meta_data = ColumnMetaData.from_data(data_x)

  categorical_encoder = construct(hypothesis.categorical_encoder)
  categorical_columns = column_meta_data.categorical_columns + column_meta_data.binary_columns
  categorical_encoder.fit(data_x, categorical_columns)
  data_x = categorical_encoder.transform(data_x, categorical_columns)
  column_meta_data = ColumnMetaData.from_data(data_x)

  data_splitter = KFold(n_splits=5, random_state=42)
  for train_index, test_index in data_splitter.split(data_x, data_y):
    train_x, train_y = data_x.loc[train_index], data_y.loc[train_index]
    test_x, test_y = data_x.loc[test_index], data_y.loc[test_index]

    categorical_imputer = construct(hypothesis.categorical_imputer)
    numeric_imputer = construct(hypothesis.numeric_imputer)

    train_x = categorical_imputer.fit_transform(train_x, column_meta_data.categorical_columns)
    train_x = numeric_imputer.fit_transform(train_x, column_meta_data.numeric_columns)

    test_x = categorical_imputer.transform(test_x, column_meta_data.categorical_columns)
    test_x = numeric_imputer.transform(test_x, column_meta_data.numeric_columns)

    outlier_remover = construct(hypothesis.outlier_remover)
    train_x, train_y = outlier_remover.transform(train_x, train_y, column_meta_data.numeric_columns)

    scaler = construct(hypothesis.scaler)
    train_x = scaler.fit_transform(train_x,
                                   column_meta_data.numeric_columns + column_meta_data.categorical_columns + column_meta_data.binary_columns)
    test_x = scaler.transform(test_x,
                              column_meta_data.numeric_columns + column_meta_data.categorical_columns + column_meta_data.binary_columns)

    pca = construct(hypothesis.pca)

    train_x = pca.fit_transform(train_x)
    test_x = pca.transform(test_x)

    model = construct(hypothesis.model)

    model.fit(train_x, train_y)
    pred_y = model.predict(test_x)

    score = accuracy_score(test_y, pred_y)
    print(score)
    scores.append(score)

  return Result(scores)


def construct(pack):
  return pack.constructor(**pack.params.__dict__)
