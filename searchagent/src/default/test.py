import pandas as pd

from sklearn.decomposition import PCA
from sklearn.ensemble import AdaBoostRegressor, BaggingRegressor, GradientBoostingRegressor, \
  RandomForestRegressor
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeRegressor
from xgboost import XGBRegressor

from sklearn.metrics import accuracy_score, mean_squared_error
from sklearn.model_selection import KFold

from data_processor.meta import ColumnMetaData
from data_processor.preprocessor import MinMaxScaler, StandardScaler, \
  NumericImputer, TukeyOutlierRemover

from model.param_space import ParamSpace
from model.result import Result
from model.hypothesis_space import HypothesisSpaceBuilder

from optimizer.bayesian import BayesianOptimizer


def get_x_y(data, response_column_name):
  return data.drop(columns=[response_column_name]), data[response_column_name]


class NoOperationFitTransformer:
  def transform(self, data_x):
    return data_x

  def fit(self, data_x):
    return data_x

  def fit_transform(self, data_x):
    return data_x


class LowVarianceFeatureRemover:
  def __init__(self, keep=15):
    self.columns = []
    self._keep = keep

  def fit(self, data_x):
    for column in data_x.columns:
      self.columns.append((data_x[column].var(), column))
    self.columns.sort(key=lambda x: x[0], reverse=True)
    self.columns = self.columns[0:self._keep]

  def transform(self, data_x):
    return data_x.drop(columns=self.columns)

  def fit_transform(self, data_x):
    self.fit(data_x)

    return self.transform(data_x)


def AdaBoostConstructor(n_estimators=10, max_depth=3, random_state=42):
  return AdaBoostRegressor(base_estimator=DecisionTreeRegressor(max_depth=max_depth, random_state=random_state),
                           n_estimators=n_estimators, random_state=random_state)


hypothesis_space = HypothesisSpaceBuilder() \
  .group(name='numeric_imputer') \
    .constructor_option(constructor=NumericImputer) \
  .group(name='outlier_remover') \
    .constructor_option(constructor=TukeyOutlierRemover) \
      .param(name='k', values=ParamSpace(1.5, 3)) \
  .group(name='scaler') \
    .constructor_option(constructor=MinMaxScaler) \
    .constructor_option(constructor=StandardScaler) \
  .group(name='pca') \
    .constructor_option(constructor=NoOperationFitTransformer) \
    .constructor_option(constructor=PCA) \
      .param(name='svd_solver', values='auto') \
      .param(name='n_components', values=ParamSpace(5, 14, is_discrete=True)) \
      .param(name='random_state', values=42) \
  .group(name='model') \
    .constructor_option(constructor=BaggingRegressor) \
      .param(name='n_estimators', values=ParamSpace(10, 200, is_discrete=True)) \
      .param(name='max_features', values=ParamSpace(0.5, 1)) \
      .param(name='max_samples', values=ParamSpace(0.5, 1)) \
      .param(name='random_state', values=42) \
    .constructor_option(constructor=AdaBoostConstructor) \
      .param(name='n_estimators', values=ParamSpace(50, 200, is_discrete=True)) \
      .param(name='max_depth', values=ParamSpace(2, 30, is_discrete=True)) \
      .param(name='random_state', values=42) \
    .constructor_option(constructor=GradientBoostingRegressor) \
      .param(name='n_estimators', values=ParamSpace(50, 200, is_discrete=True)) \
      .param(name='max_depth', values=ParamSpace(2, 30, is_discrete=True)) \
      .param(name='subsample', values=ParamSpace(0.5, 1)) \
      .param(name='min_samples_leaf', values=ParamSpace(1, 150, is_discrete=True)) \
      .param(name='random_state', values=42) \
    .constructor_option(constructor=RandomForestRegressor) \
      .param(name='n_estimators', values=ParamSpace(50, 200, is_discrete=True)) \
      .param(name='criterion', values='mse') \
      .param(name='max_depth', values=ParamSpace(2, 30, is_discrete=True)) \
      .param(name='random_state', values=42) \
      .param(name='max_features', values=ParamSpace(0.5, 1)) \
      .param(name='min_samples_leaf', values=ParamSpace(1, 150, is_discrete=True)) \
    .constructor_option(constructor=LogisticRegression) \
      .param(name='penalty', values=['l1', 'l2']) \
      .param(name='C', values=ParamSpace(1.0, 20.0)) \
      .param(name='random_state', values=42) \
    .constructor_option(constructor=XGBRegressor) \
      .param(name='random_state', values=42) \
      .param(name='verbosity', values=0) \
      .param(name='max_depth', values=ParamSpace(2, 30, is_discrete=True)) \
      .param(name='n_estimators', values=ParamSpace(50, 200, is_discrete=True)) \
      .param(name='subsample', values=ParamSpace(0.5, 1)) \
  .build()


class Optimizer(BayesianOptimizer):
  def __init__(self, params_to_optimize, blackbox):
    super().__init__(params_to_optimize, blackbox, init_points=2, n_iter=2)


param_optimizer = Optimizer


def hypothesis_pipeline(dataset_path, hypothesis):
  scores = []

  data = pd.read_csv(dataset_path, index_col=False)

  data_x, data_y = get_x_y(data, 'target')

  feature_remover = LowVarianceFeatureRemover()
  data_x = feature_remover.fit_transform(data_x)

  column_meta_data = ColumnMetaData.from_data(data_x)

  data_splitter = KFold(n_splits=5, random_state=42)
  for train_index, test_index in data_splitter.split(data_x, data_y):
    train_x, train_y = data_x.loc[train_index], data_y.loc[train_index]
    test_x, test_y = data_x.loc[test_index], data_y.loc[test_index]

    numeric_imputer = construct(hypothesis.numeric_imputer)

    train_x = numeric_imputer.fit_transform(train_x, column_meta_data.numeric_columns)
    test_x = numeric_imputer.transform(test_x, column_meta_data.numeric_columns)

    outlier_remover = construct(hypothesis.outlier_remover)
    train_x, train_y = outlier_remover.transform(train_x, train_y, column_meta_data.numeric_columns)

    scaler = construct(hypothesis.scaler)
    train_x = scaler.fit_transform(train_x, column_meta_data.numeric_columns + column_meta_data.categorical_columns + column_meta_data.binary_columns)
    test_x = scaler.transform(test_x, column_meta_data.numeric_columns + column_meta_data.categorical_columns + column_meta_data.binary_columns)

    pca = construct(hypothesis.pca)
    train_x = pca.fit_transform(train_x)
    test_x = pca.transform(test_x)

    model = construct(hypothesis.model)

    model.fit(train_x, train_y)
    pred_y = model.predict(test_x)

    scores.append(mean_squared_error(test_y, pred_y))

  return Result(scores)


def construct(pack):
  return pack.constructor(**pack.params.__dict__)
