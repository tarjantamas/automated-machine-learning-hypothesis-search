# Local files

dataset_store_path = '../resources/datasets'

search_tasks_path = 'downloaded_sources'

agent_info_path = '../resources/agent.info'

# Server endpoints

server_root_endpoint = 'http://167.71.32.67:45569/api/v1'

results_endpoint = server_root_endpoint + "/results"

agents_endpoint = server_root_endpoint + "/agents"

agent_endpoint = server_root_endpoint + "/agent"

task_frames_endpoint = server_root_endpoint + "/task-frames"

dataset_endpoint = server_root_endpoint + "/dataset"

search_task_endpoint = server_root_endpoint + "/search-task"

agent_logs_endpoint = server_root_endpoint + "/agent-logs"
