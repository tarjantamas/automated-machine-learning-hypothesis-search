from model.choice_space import Choice, FixedChoice, ChoiceSpace
from model.param_space import ParamSpace


class HypothesisSpaceBuilder:  
  def __init__(self):
    self._groups = []
    self._current_group = None
    self._current_constructor = None

  def group(self, name):
    self._current_group = Group(name)
    self._groups.append(self._current_group)
  
    return self

  def constructor_option(self, constructor):
    self._current_constructor = Constructor(constructor)
    self._current_group.options.append(self._current_constructor)

    return self

  def param(self, name=None, values=None):
    self._current_constructor.params.append(Param(name, values))

    return self

  def build(self):
    choices = list(map(group_to_choice, self._groups))

    hypothesis_space = ChoiceSpace(choices=choices)

    hypothesis_space.initialize()
    
    return hypothesis_space


def group_to_choice(group):
  options = list(map(constructor_to_choice_space, group.options))

  return Choice(name=group.name, options=options)


def constructor_to_choice_space(option):
  param_choices = list(map(param_to_choice, option.params))

  return ChoiceSpace(choices=[
    FixedChoice(name='constructor', value=option.constructor),
    ChoiceSpace(name='params', choices=param_choices)
  ])


def param_to_choice(param):
  if isinstance(param.values, list):
    return Choice(name=param.name, options=param.values)
  else:
    return FixedChoice(name=param.name, value=param.values)


class Group:
  def __init__(self, name):
    self.name = name
    self.options = []

  def __repr__(self):
    return str(self.__dict__)


class Constructor:
  def __init__(self, constructor):
    self.constructor = constructor
    self.params = []

  def __repr__(self):
    return str(self.__dict__)


class Param:
  def __init__(self, name, values):
    self.name = name
    self.values = values

  def __repr__(self):
    return str(self.__dict__)