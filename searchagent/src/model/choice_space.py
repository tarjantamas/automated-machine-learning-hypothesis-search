from functools import reduce
import operator


class Printable:
  def __repr__(self):
    return str(self.__dict__)


def dict_to_object(dictionary):
  result = SimpleObject()

  for key in dictionary:
    value = dictionary[key]

    if isinstance(value, dict):
      value = dict_to_object(value)
    elif isinstance(value, list):
      value = list(map(dict_to_object, value))
    
    result.__dict__[key] = value
  
  return result


class SimpleObject(Printable):
  pass


class Boundary(Printable):
  def __init__(self, start, end):
    self.start = start
    self.end = end
  
  def contains(self, value):
    return value >= self.start and value <= self.end


class IndexedChoice(Printable):
  def __init__(self, name=''):
    self.name = name

  def __getitem__(self, index):
    pass

  def __len__(self):
    pass

  def _wrap_result(self, result):
    wrapper = {}
    wrapper[self.name] = result
    return wrapper


class Choice(IndexedChoice):
  def __init__(self, name='', options=[]):
    super().__init__(name)
    self._options = options
    self._change_interval = 1
    self._length = sum(map(lambda x: _length(x), self._options))
    self._boundries = self._get_boundries()

  def __len__(self):
    return self._length
  
  def __getitem__(self, index):
    index = (index // self._change_interval) % len(self)
    boundary, base_index = self._get_boundry(index)
    option = self._options[base_index]
    if isinstance(option, IndexedChoice):
      option = option[index - boundary.start]
    return self._wrap_result(option)

  def string_id(self, index):
    index = (index // self._change_interval) % len(self)
    boundry, base_index = self._get_boundry(index)
    option = self._options[base_index]
    if isinstance(option, IndexedChoice):
      base_index = option.string_id(index - boundry.start)
    return base_index

  def _get_boundry(self, index):
    for i in range(len(self._boundries)):
      if self._boundries[i].contains(index):
        return self._boundries[i], i
    raise Exception('Could not find valid boundry for index: {}'.format(index))

  def _get_boundries(self):
    boundries = []
    start = 0
    for option in self._options:
      end = start + _length(option)
      boundries.append(Boundary(start, end - 1))
      start = end
    return boundries

  def _initialize_change_interval(self, current_change_interval=1):
    self._change_interval = current_change_interval
    for option in self._options:
      if isinstance(option, IndexedChoice):
        option._initialize_change_interval()
    return current_change_interval * len(self)


class FixedChoice(Choice):
  def __init__(self, name='', value=''):
    super().__init__(name=name, options=[value])


class ChoiceSpace(IndexedChoice):
  def __init__(self, name='', choices=[]):
    super().__init__(name)
    self._choices = choices
    self._length = reduce(operator.mul, map(len, self._choices), 1)
  
  def __len__(self):
    return self._length
  
  def __getitem__(self, index):
    result = {}
    for choice in self._choices:
      result.update(choice[index])
    if self.name != '':
      return self._wrap_result(result)
    return result

  def string_id(self, index):
    result = ''
    for choice in self._choices:
      result += str(choice.string_id(index))
    return result

  def initialize(self):
    self._initialize_change_interval()

  def _initialize_change_interval(self, current_change_interval=1):
    for choice in reversed(self._choices):
      current_change_interval = choice._initialize_change_interval(current_change_interval)
    return current_change_interval


def _length(obj):
  if isinstance(obj, IndexedChoice):
    return len(obj)
  return 1
