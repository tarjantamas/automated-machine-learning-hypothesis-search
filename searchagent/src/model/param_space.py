class ParamSpace:
  def __init__(self, start, end, is_discrete=False):
    self.start = start
    self.end = end
    self.is_discrete = is_discrete

  @staticmethod
  def discretize_discrete_params(params, param_spaces):
    for param_name in params:
      param_space = param_spaces[param_name]
      if param_space.is_discrete:
        params[param_name] = int(round(params[param_name]))
