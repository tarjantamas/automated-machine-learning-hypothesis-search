class ColumnType:
  BINARY = 'binary'
  CATEGORICAL = 'categorical'
  CONTINUOUS = 'continuous'
  DISCRETE = 'discrete'

  @staticmethod
  def from_column(column):
    if column.dtype == 'object':
      unique_value_count = len(column.unique())  
      if unique_value_count <= 2:
        return ColumnType.BINARY
      else:
        return ColumnType.CATEGORICAL
    
    elif column.dtype == 'float64':
      return ColumnType.CONTINUOUS

    elif column.dtype == 'int64':
      return ColumnType.DISCRETE

  @staticmethod
  def from_data(data):
    columns = map(lambda column_name: data[column_name], data.columns)
    return list(map(ColumnType.from_column, columns))


class DataColumnTypeIndex:
  def __init__(self, data):
    self.columns = {}

    column_types = ColumnType.from_data(data)

    for column_type in column_types:
      self.columns[column_type] = []

    for column_name, column_type in zip(data.columns, column_types):
      self.columns[column_type].append(column_name)

  def get_columns_by_type(self, column_type):
    if column_type not in self.columns:
      return []
    return self.columns[column_type]
