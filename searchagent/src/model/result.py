import numpy as np

class Result:
  def __init__(self, results=[]):
    self.min = float(np.min(results))
    self.max = float(np.max(results))
    self.median = float(np.median(results))
    self.mean = float(np.mean(results))
    self.std = float(np.std(results))
    self.hypothesis = ''

  def __repr__(self):
    return str(self.__dict__)
