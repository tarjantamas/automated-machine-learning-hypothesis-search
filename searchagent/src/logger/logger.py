class Logger:
  def __init__(self, agent_log_service):
    self.agent_log_service = agent_log_service
  
  def log(self, context="UNKOWN CONTEXT", text=''):
    log_text = "{}: {}".format(context, text)
    print(log_text)

    if self.agent_log_service.agent_uuid is not None:
      self.agent_log_service.create({ 'text': log_text })
  
  def get_logger(self, context):
    return ContextualizedLogger(context, self.agent_log_service)

class ContextualizedLogger(Logger):
  def __init__(self, context, agent_log_service):
    super().__init__(agent_log_service)
    self.context = context

  def log(self, text):
    super().log(self.context, text)

  def exception(self, text, traceback):
    log_text = "{}: {}".format(self.context, text)

    print(traceback)
    print(log_text)

    if self.agent_log_service.agent_uuid is not None:
      self.agent_log_service.create({ 'text': log_text, 'traceback': { 'text': traceback } })