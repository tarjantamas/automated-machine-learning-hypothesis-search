from scipy.spatial.distance import cdist
from collections import Counter
from sklearn.impute import SimpleImputer
import numpy as np
import pandas as pd


class KNNImputer:
  def __init__(self, k=7, aggregation_methods={
      'gender': 'vote', 
      'parental level of education': 'vote',
      'lunch': 'vote',
      'test preparation course': 'vote',
      'math score': 'mean',
      'reading score': 'mean',
      'writing score': 'mean'}):

    self.neighbours = None
    self.k = k
    self.aggregation_methods = aggregation_methods
    self.simple_imputers = {}
  
  def fit(self, train_x):
    for column_name in train_x.columns:
      if self.aggregation_methods[column_name] == 'mean':
        self.simple_imputers[column_name] = SimpleImputer(missing_values=np.nan, strategy='mean')
      else:
        self.simple_imputers[column_name] = SimpleImputer(missing_values=np.nan, strategy='most_frequent')
      self.simple_imputers[column_name].fit(train_x[[column_name]])
    self.neighbours = train_x.dropna(axis=0, how='any')

  def transform(self, data_x):
    for targetColumn in data_x.columns:
      neighbours = self.neighbours.drop(targetColumn, axis=1)
      missing_data = data_x.loc[data_x.isnull().any(axis=1)] \
                         .drop(targetColumn, axis=1)
      if len(missing_data) == 0:
        continue

      for column_name in missing_data.columns:
        missing_data[column_name] = self.simple_imputers[column_name].transform(missing_data[[column_name]]).ravel()

      distances = cdist(missing_data, neighbours, metric='euclidean')

      for index, dataFrameIndex in enumerate(missing_data.index):
        value = data_x.loc[dataFrameIndex]
        nearest_neighbour_indices = filter(lambda x: x != np.nan, distances[index].argsort()[:self.k])
        nearest_neighbour_indices = list(map(lambda x: self.neighbours.index[x], nearest_neighbour_indices))
        if len(nearest_neighbour_indices) == 0:
          continue

        nearest_neighbours = self.neighbours.loc[nearest_neighbour_indices]
        aggregated_values = self.aggregate_neighbours(nearest_neighbours)
        for column_name in data_x.columns:
          if pd.isnull(value[column_name]):
            data_x.loc[dataFrameIndex, column_name] = aggregated_values[column_name]

  def aggregate_neighbours(self, neighbours):
    aggregated_values = {}
    for column_name in neighbours.columns:
      if self.aggregation_methods[column_name] == 'vote':
        aggregated_values[column_name] = self.vote(neighbours[column_name])
      else:
        aggregated_values[column_name] = np.mean(neighbours[column_name])
    return aggregated_values

  def vote(self, values):
    counter = Counter()
    for value in values:
      counter[value] += 1
    return counter.most_common(n=1)[0][0]

  def fit_transform(self, train_x):
    self.fit(train_x)
    return self.transform(train_x)