from model.column_meta import DataColumnTypeIndex, ColumnType



class ColumnMetaData:
  def __init__(self):
    self.binary_columns = []
    self.categorical_columns = []
    self.numeric_columns = []
  
  @staticmethod
  def from_data(data):
    data_column_type_index = DataColumnTypeIndex(data)
    categorical_columns = data_column_type_index.get_columns_by_type(ColumnType.CATEGORICAL)
    binary_columns = data_column_type_index.get_columns_by_type(ColumnType.BINARY)
    numeric_columns = data_column_type_index.get_columns_by_type(ColumnType.CONTINUOUS) + \
                      data_column_type_index.get_columns_by_type(ColumnType.DISCRETE)
    
    column_meta = ColumnMetaData()
    column_meta.binary_columns = binary_columns
    column_meta.numeric_columns = numeric_columns
    column_meta.categorical_columns = categorical_columns

    return column_meta