import pandas as pd
import numpy as np


class FitTransformer:
  def fit(self, *params):
    pass

  def transform(self, *params):
    pass

  def fit_transform(self, *params):
    self.fit(*params)

    return self.transform(*params)


class MeanImputer(FitTransformer):
  def __init__(self):
    self.mean = 0

  def fit(self, train_x):
    self.mean = train_x.mean()

  def transform(self, train_x):
    return train_x.fillna(value=self.mean)


class MostFrequentImputer(FitTransformer):
  def __init__(self):
    self.most_frequent = ''

  def fit(self, train_x):
    self.most_frequent = train_x.value_counts().idxmax()

  def transform(self, train_x):
    return train_x.fillna(value=self.most_frequent)


class CategoricalImputer(FitTransformer):
  def __init__(self):
    self.imputers = {}

  def fit(self, data, column_names):
    for column_name in column_names:
      simple_imputer = MostFrequentImputer()
      simple_imputer.fit(data[column_name])
      self.imputers[column_name] = simple_imputer

  def transform(self, data, column_names):
    data = data.copy()

    for column_name in column_names:
      data[column_name] = self.imputers[column_name].transform(data[column_name])

    return data


class NumericImputer(CategoricalImputer):
  def fit(self, data, column_names):
    for column_name in column_names:
      simple_imputer = MeanImputer()
      simple_imputer.fit(data[column_name])
      self.imputers[column_name] = simple_imputer


class OneHotEncoder(FitTransformer):
  def __init__(self):
    self.column_dummies = {}
  
  def fit(self, data, column_names):
    self.column_dummies = {}
    for column_name in column_names:
      self.column_dummies[column_name] = pd.get_dummies(data[column_name], prefix=column_name, dummy_na=True)

  def transform(self, data, column_names):
    for column_name in column_names:
      dummies = self.column_dummies[column_name]
      data = pd.concat([data, dummies], axis=1)
      data = data.drop(columns=[column_name])

    return data


class OrdinalEncoder(FitTransformer):
  def __init__(self):
    self.unique_column_values = {}

  def fit(self, data, column_names):
    for column_name in column_names:
      unique_values = data[column_name].unique()
      self.unique_column_values[column_name] = unique_values
 
  def transform(self, data, column_names):
    for column_name in column_names:
      for i, value in enumerate(self.unique_column_values[column_name]):
        data[column_name] = data[column_name].replace(value, i)

    return data


class BinaryEncoder(FitTransformer):
  def __init__(self):
    self._column_replace_dicts = {}

  def fit(self, data, column_names):
    for column_name in column_names:
      self._column_replace_dicts[column_name] = self._get_replace_dict(data, column_name)

  def transform(self, data, column_names):
    for column_name in column_names:
      data[column_name] = data[column_name].replace(to_replace=self._column_replace_dicts[column_name])

    return data

  def _get_replace_dict(self, data, column_name):
    replace_dict = {}
    values = data[column_name].unique()
    values.sort()

    if len(values) > 2:
      raise Exception("BinaryEncoder used on column `{}` that has more than 2 unique values.".format(column_name))

    replace_dict[values[0]] = 0
    replace_dict[values[1]] = 1

    return replace_dict


class StandardScaler(FitTransformer):
  def __init__(self):
    self._column_means = {}
    self._column_stds = {}

  def fit(self, data, column_names):
    for column_name in column_names:
      values = data[column_name]
      self._column_means[column_name] = np.mean(values)
      self._column_stds[column_name] = np.std(values)
  
  def transform(self, data, column_names):
    for column_name in column_names:
      values = data[column_name]
      std = self._column_stds[column_name]

      if std == 0:
        data[column_name] = values * 0
      else:
        data[column_name] = (values - self._column_means[column_name]) / std

    return data


class MinMaxScaler(FitTransformer):
  def __init__(self):
    self.column_mins = {}
    self.column_maxs = {}

  def fit(self, data, column_names):
    for column_name in column_names:
      values = data[column_name]
      self.column_mins[column_name] = np.min(values)
      self.column_maxs[column_name] = np.max(values)
  
  def transform(self, data, column_names):
    for column_name in column_names:
      values = data[column_name]
      value_range = self.column_maxs[column_name] - self.column_mins[column_name]
      if value_range == 0:
        data[column_name] = values * 0
      else:
        data[column_name] = (values - self.column_mins[column_name]) / value_range

    return data


class TukeyOutlierRemover:
  def __init__(self, k=1.5):
    self._k = k

  def transform(self, data_x, data_y, column_names):
    for column_name in column_names:
      column = data_x[column_name]

      q1 = column.quantile(0.25)
      q2 = column.quantile(0.75)

      iqr = q2 - q1

      fence_low = q1 - self._k * iqr
      fence_high = q2 + self._k * iqr

      index = (column >= fence_low) & (column <= fence_high)
      data_x = data_x.loc[index]
      data_y = data_y.loc[index]

    return data_x.reset_index(drop=True), data_y.reset_index(drop=True)

