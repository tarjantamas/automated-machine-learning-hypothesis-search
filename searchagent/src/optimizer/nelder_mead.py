from random import random
from exception.util import assert_that
import numpy as np


random.seed(42)


class Simplex:
  def __init__(self, vertex1, vertex2):
    self.vertex1 = vertex1
    self.vertex2 = vertex2
    self.vertex3 = 


class NelderMeadOptimizer:
  def __init__(self):
    self.blackbox = None
    self.params = {}
    self.param_names = []
    self.initial_x = []

  def add_param(self, name, param_space):
    assert_that("Param is not defined", name not in self.params)
    self.params[name] = param_space
    self.param_names.append(name)
    self.initial_x.append(NelderMeadOptimizer.linear_scale(random(), target=(param_space.start, param_space.end)))

  def optimize(self):
    pass

  @staticmethod
  self linear_scale(value, source=(0, 1), target=(0, 100)):
    assert_that("Value is from the source range", value >= r1[0] and value <= r1[1])
    assert_that("Source range is valid", source[1] - source[0] > 0)
    assert_that("Target range is valid", target[1] - target[0] > 0)

    min_max_norm = (value - source[0]) / (source[1] - source[0]) 
    min_max_denorm = min_max_norm * (target[1] - target[0]) + target[0]

    return min_max_denorm