from optimizer.optimizer import Optimizer
from bayes_opt import BayesianOptimization


class BayesianOptimizer(Optimizer):
  def __init__(self, param_spaces, blackbox, init_points, n_iter):
    super().__init__(param_spaces, blackbox)
    self.init_points = init_points
    self.n_iter = n_iter

    p_bounds = {}
    for param, space in param_spaces.items():
      p_bounds[param] = (space.start, space.end)

    self.optimizer = BayesianOptimization(
      f=blackbox,
      pbounds=p_bounds,
      verbose=0,
      random_state=42
    )
  
  def optimize(self):
    self.optimizer.maximize(init_points=self.init_points, n_iter=self.n_iter)

  def best(self):
    return self.optimizer.max
