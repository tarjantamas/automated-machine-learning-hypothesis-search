class Optimizer:
  def __init__(self, param_spaces, blackbox):
    """
      params_to_optimize should be a dictionary containing
      parameter names as keys that map to ParamSpace objects
      which define the search space for each parameter.
    """
    self.param_spaces = param_spaces
    self.blackbox = blackbox
  
  def optimize(self):
    pass
  
  def best(self):
    """
      Return a dictionary containing a 'target' key 
      which maps to the best result found during the 
      optimization process. The dictionary should also 
      contain 'params' key which maps to another dictionary
      containing parameter names as keys that map to their
      respective parameter values.
    """
    pass