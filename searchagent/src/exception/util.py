def assert_that(message, assertion):
  if not assertion:
    raise Exception("Assertion error: `{}`".format(message))
