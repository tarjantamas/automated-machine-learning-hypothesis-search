# Start dockerized agent

Run ```docker run -d tamastarjan/agent:1.0```

# Print size of the hypothesis space

Install dependencies using ```pip install -r requirements.txt```.
Place your search_task.py file in ./src/default.
Run ```python main.py --size``` from ./src.

# Build docker image

```docker build -t agent:1.0 .```
```docker tag agent:1.0 username/agent:1.0```
```docker login```
```docker push username/agent:1.0```
Delete old image ```docker image rm image_id```